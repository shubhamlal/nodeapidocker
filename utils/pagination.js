const check = require('./../lib/checkLib');

let paginationWithFromTo = (searchParameter, fromParameter, toParameter, setOrder, filterFromRange, filterToRange) => {
    let search = check.isEmpty(searchParameter) ? "" : searchParameter;
    let from = check.isEmpty(fromParameter) ? 1 : fromParameter;
    let to = check.isEmpty(toParameter) ? 10 : toParameter;
    let order = check.isEmpty(setOrder) ? "" : setOrder;
    let filterFrom = check.isEmpty(filterFromRange) ? "" : filterFromRange;
    let filterTo = check.isEmpty(filterToRange) ? "" : filterToRange;
    let pageSize = (to - from) + 1;
    let offset = from - 1;
    return { search, offset, pageSize, order, filterFrom, filterTo };
}

let paginationWithPageNumberPageSize = (searchParameter, pageNumberParameter, pageSizeParameter) => {
    let search = check.isEmpty(searchParameter) ? "" : searchParameter;
    let pageNumber = !check.isEmpty(pageNumberParameter) ? pageNumberParameter : 1;
    let pageSize = pageSizeParameter || 10;
    if (pageSize != 0 && pageNumber != 0) {
        userOffset = (pageSize * (pageNumber - 1));
    }
    return { search, userOffset, pageSize, pageNumber };
}

let NextAndPrevPageNumber = (pageNumberParameter, pageSizeParameter, userCount) => {
    let currentObject = pageNumberParameter * pageSizeParameter;
    let prev = currentObject == pageSizeParameter ? null : Number(pageNumberParameter) - 1;
    let next = currentObject >= userCount ? null : Number(pageNumberParameter) + 1;
    let lastPage = Math.ceil(userCount / pageSizeParameter);

    return { next, prev, lastPage };

}

module.exports = {
    paginationWithFromTo: paginationWithFromTo,
    paginationWithPageNumberPageSize: paginationWithPageNumberPageSize,
    NextAndPrevPageNumber: NextAndPrevPageNumber
}