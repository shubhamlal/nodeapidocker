const roles = require('../api/controller/roles');
const express =require('express');
const checkAuthUser = require('../api/Middleware/checkAuth');
const  { roleValidation }  = require('../api/validation/roles'); 
const validationError = require('../api/Middleware/validationError'); // importing validation error middleware.
const errorWrap = require('../utils/errorWrap'); // importing check authentication.



const route = express.Router();

route.get('/',checkAuthUser,errorWrap.wrapper(roles.GetRolesData)); //api to get roles

route.get('/:id',checkAuthUser,errorWrap.wrapper(roles.GetRoleById));//api to get role bt id

route.put('/:id',checkAuthUser,roleValidation,validationError,errorWrap.wrapper(roles.UpdateRole)); // api to create role

route.post('/', checkAuthUser,roleValidation,validationError,errorWrap.wrapper(roles.CreateRole)); //api to create roles

route.delete('/:id',checkAuthUser,errorWrap.wrapper(roles.RemoveRole));//api to remove role

module.exports= route;

/**
* @swagger
* /roles:
*   get:
*     tags:
*       - roles
*     name: roles APIs
*     summary: To read roles
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Data found 
*       404:
*         description: Data not found
*   post:
*     tags:
*       - roles
*     summary: To add roles
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             roleName:
*               type: string
*             createdby:
*               type: integer
*         required:
*           - permissionName
*     responses:
*       201:
*         description: role created
*       422:
*         description: role not created
* /roles/{id}:
*   put:
*     tags:
*       - roles
*     summary: To update roles
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: "id"
*         in: "path"
*         description: "Id of role to update"
*         required: true
*         type: "integer"
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             roleName:
*               type: string
*             createdby:
*               type: integer
*     responses:
*       200:
*         description: role updated
*       404:
*         description: role you are updating does not exit
*   delete:
*     tags:
*       - roles
*     summary: To delete roles by Id
*     parameters:
*     - name: "id"
*       in: "path"
*       description: "Id of roles to delete"
*       required: true
*       type: "integer"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: role deleted. 
*       404:    
*         description: The role does not exit.
*/
