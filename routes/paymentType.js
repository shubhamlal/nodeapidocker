const paymentType = require('../api/controller/paymentType');
const express =require('express');
const checkAuthUser = require('../api/Middleware/checkAuth');

const errorWrap = require('../utils/errorWrap'); // importing check authentication.



const route = express.Router();

route.post('/',checkAuthUser,errorWrap.wrapper(paymentType.addPaymentType)); // add payment type

route.get('/',checkAuthUser,errorWrap.wrapper(paymentType.GetPaymentType)); //get payment type

route.get('/:id',checkAuthUser,errorWrap.wrapper(paymentType.GetPaymentTypeById));//get payment type by id

route.put('/:id',checkAuthUser,errorWrap.wrapper(paymentType.UpdatePaymentType)); //update payment type by id

route.delete('/:id',checkAuthUser,errorWrap.wrapper(paymentType.RemovePaymentType)); //remove payment type

module.exports= route;

/**
* @swagger
* /payment-type:
*   get:
*     tags:
*       - payment type
*     name: payment type APIs
*     summary: To read payment type
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Data found 
*       404:
*         description: Data not found
*   post:
*     tags:
*       - payment type
*     summary: To add payment type
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             paymentType:
*               type: string
*         required:
*           - paymentType
*     responses:
*       201:
*         description: paymentType created
*       422:
*         description: paymentType not created
* /payment-type/{id}:
*   put:
*     tags:
*       - payment type
*     summary: To update payment type
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: "id"
*         in: "path"
*         description: "Id of payment type to update"
*         required: true
*         type: "integer"
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             paymentType:
*               type: string
*     responses:
*       200:
*         description: payment type updated
*       404:
*         description: payment type you are updating does not exit
*   delete:
*     tags:
*       - payment type
*     summary: To delete payment type by Id
*     parameters:
*     - name: "id"
*       in: "path"
*       description: "Id of payment type to delete"
*       required: true
*       type: "integer"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: payment type deleted. 
*       404:    
*         description: The payment type does not exit.
*/