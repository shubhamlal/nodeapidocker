
const user = require('../api/controller/users');
const express =require('express');
const errorWrap = require('../utils/errorWrap'); // importing check authentication.

const route = express.Router();

route.post('/',errorWrap.wrapper(user.signInAdmin)); //signin user

module.exports= route;


/**
* @swagger
* /signIn:
*   post:
*     tags:
*       - signIn
*     summary: To signIn
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             email:
*               type: string
*             password:
*               type: string
*     responses:
*       200:
*         description: Authenticate Successfully
*       422:
*         description: Wrong UserId or Password
*/