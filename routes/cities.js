var express = require('express');
var router = express.Router();

const { postCity, getCity } = require('../api/controller/cities')

const checkAuth = require('../api/Middleware/checkAuth');

const errorWrap = require('../utils/errorWrap'); // importing check authentication.

// const { wrapper } = require('../utils/errorWrap');


const multer = require('multer');
const storage = multer.diskStorage({
    destination: 'public/uploads/csv',
    filename: (req, file, cb) => {
        const extArray = file.originalname.split('.');
        const extension = extArray[extArray.length - 1];
        cb(null, `${Date.now()}.${extension}`)
    }
})
const upload = multer({
    storage: storage
});

router.post('/', checkAuth, upload.single('csv'), postCity);

router.get('/:stateId', checkAuth, errorWrap.wrapper(getCity));

module.exports = router;

/**
* @swagger
* /cities/{id}:
*   get:
*     tags:
*       - Cities
*     summary: To read cities by Id
*     parameters:
*     - name: "id"
*       in: "path"
*       description: "ID of cities to return"
*       required: true
*       type: "integer"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Product found. 
*       404:    
*         description: The product does not exit.
*/