
const bulkUpload = require('../api/controller/bulkUpload');
const express =require('express');
const route = express.Router();
const checkAuthUser = require('../api/Middleware/checkAuth')


route.get('/',checkAuthUser,bulkUpload.getBulkUploadFile); // api to get Files.

route.get('/get-bulk-report/by-file-id',checkAuthUser, bulkUpload.getBulkReport);//api to get bulk upload report.

route.post('/',checkAuthUser,bulkUpload.SaveRequestBulkUploadFiles); //api for File Upload.

module.exports= route;
