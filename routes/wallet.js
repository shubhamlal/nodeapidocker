
const wallet = require('../api/controller/wallet');
const express =require('express');
const checkAuthUser = require('../api/Middleware/checkAuth');
const errorWrap = require('../utils/errorWrap'); // importing check authentication.



const route = express.Router();

route.post('/',checkAuthUser,errorWrap.wrapper(wallet.AddWalletPrice)); // api to create wallet.

route.get('/',checkAuthUser,errorWrap.wrapper(wallet.GetWalletData));// api to read wallet data.

route.put('/:id',checkAuthUser,errorWrap.wrapper(wallet.UpdateWallet));// api to update wallet data.

module.exports= route;

/**
* @swagger
* /wallet:
*   get:
*     tags:
*       - wallet
*     name: wallet APIs
*     summary: To read wallet
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Data found 
*       404:
*         description: Data not found
*   post:
*     tags:
*       - wallet
*     summary: To add wallet
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             metalType:
*               type: string
*             walletPrice:
*               type: integer
*               format: float
*             forwordCostThreeMonth:
*               type: integer
*               format: float
*             forwordCostSixMonth:
*               type: integer
*               format: float
*             forwordCostNineMonth:
*               type: integer
*               format: float
*             vat:
*               type: integer
*               format: float
*             excixeDuty:
*               type: integer
*               format: float
*             cancelValue:
*               type: integer
*               format: float
*         required:
*           - permissionName
*     responses:
*       201:
*         description: role created
*       422:
*         description: role not created
* /wallet/{id}:
*   put:
*     tags:
*       - wallet
*     summary: To update roles
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: "id"
*         in: "path"
*         description: "Id of role to update"
*         required: true
*         type: "integer"
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             metalType:
*               type: integer
*             walletPrice:
*               type: integer
*               format: float
*             forwordCostThreeMonth:
*               type: integer
*               format: float
*             forwordCostSixMonth:
*               type: integer
*               format: float
*             forwordCostNineMonth:
*               type: integer
*               format: float
*             vat:
*               type: integer
*               format: float
*             excixeDuty:
*               type: integer
*               format: float
*             cancelValue:
*               type: integer
*               format: float
*     responses:
*       200:
*         description: role updated
*       404:
*         description: role you are updating does not exit
*/