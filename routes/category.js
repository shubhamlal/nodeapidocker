
const category = require('../api/controller/category');
const express =require('express');
const checkAuthUser = require('../api/Middleware/checkAuth')

const  { categoryValidation }  = require('../api/validation/category'); 

const validationError = require('../api/Middleware/validationError'); // importing validation error middleware.

const errorWrap = require('../utils/errorWrap'); // importing check authentication.

const route = express.Router();

route.get("/", checkAuthUser,errorWrap.wrapper(category.getAllCategory));// api to get all Category.

route.post('/',checkAuthUser,categoryValidation,validationError,errorWrap.wrapper(category.AddCategory)); // api to create Category.

route.patch('/:id',checkAuthUser,categoryValidation,validationError,errorWrap.wrapper(category.UpdateCategory));

route.delete('/:id',checkAuthUser,errorWrap.wrapper(category.deleteAnyOneCategory));

module.exports= route;

/**
* @swagger
* /category:
*   get:
*     tags:
*       - Category
*     name: Read Category
*     summary: To read Category with pagination
*     parameters:
*     - name: "search"
*       in: "query"
*       description: "search your keyword"
*       type: "string"
*     - name: "from"
*       in: "query"
*       description: "Pagination starting point"
*       type: "string"
*     - name: "to"
*       in: "query"
*       description: "Pagination ending point"
*       type: "string"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Data found 
*       404:
*         description: Data not found
*   post:
*     tags:
*       - Category
*     summary: To add Category
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             categoryName:
*               type: string
*             createdby:
*               type: integer
*         required:
*           - categoryName
*     responses:
*       201:
*         description: Category created
*       422:
*         description: Category not created
* /category/{id}:
*   patch:
*     tags:
*       - Category
*     summary: To update Category
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: "id"
*         in: "path"
*         description: "Id of Category to update"
*         required: true
*         type: "integer"
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             categoryName:
*               type: string
*             createdby:
*               type: integer
*     responses:
*       200:
*         description: Category updated
*       404:
*         description: Category you are updating does not exit
*   delete:
*     tags:
*       - Category
*     summary: To delete Category by Id
*     parameters:
*     - name: "id"
*       in: "path"
*       description: "Id of category to delete"
*       required: true
*       type: "integer"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Category deleted. 
*       404:    
*         description: The category does not exit.
*/