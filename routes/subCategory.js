const subCategory = require('../api/controller/subCategory');
const express =require('express');
const errorWrap = require('../utils/errorWrap'); // importing check authentication.

const route = express.Router();

const  {subCategoryValidation}  = require('../api/validation/subcategory'); 

const validationError = require('../api/Middleware/validationError'); // importing validation error middleware.

const checkAuthUser = require('../api/Middleware/checkAuth')

route.post('/',checkAuthUser,subCategoryValidation,validationError,errorWrap.wrapper(subCategory.AddSubCategory));// api to create subCategory.

route.get('/',checkAuthUser,errorWrap.wrapper(subCategory.ReadSubCategory)); // api to read SubCategory.

route.get('/:id',checkAuthUser,errorWrap.wrapper(subCategory.ReadSubCategoryById));// api to read one SubCategory.

route.put('/:id',checkAuthUser,subCategoryValidation,validationError,errorWrap.wrapper(subCategory.UpdateSubCategory))// api to update SubCategory.

route.delete('/:id',checkAuthUser,errorWrap.wrapper(subCategory.RemoveSubCategory))// api to remove SubCategory.

module.exports= route;

/**
* @swagger
* /sub-category:
*   get:
*     tags:
*       - Sub-category
*     name: Read Sub-category
*     summary: To read Sub-category with pagination
*     parameters:
*     - name: "search"
*       in: "query"
*       description: "search your keyword"
*       type: "string"
*     - name: "from"
*       in: "query"
*       description: "Pagination starting point"
*       type: "string"
*     - name: "to"
*       in: "query"
*       description: "Pagination ending point"
*       type: "string"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Data found 
*       404:
*         description: Data not found
*   post:
*     tags:
*       - Sub-category
*     summary: To add Sub-category
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             subCategoryName:
*               type: string
*             categoryId:
*               type: integer
*             createdby:
*               type: integer
*     responses:
*       201:
*         description: Product created
*       422:
*         description: Product not created
* /sub-category/{id}:
*   get:
*     tags:
*       - Sub-category
*     name: Read Sub-category
*     summary: To read Sub-category by Id
*     parameters:
*     - name: "id"
*       in: "path"
*       description: "ID of sub-category to return"
*       required: true
*       type: "integer"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Data found 
*       404:    
*         description: Data not found
*   put:
*     tags:
*       - Sub-category
*     summary: To update Sub-category
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: "id"
*         in: "path"
*         description: "Id of sub-category to update"
*         required: true
*         type: "integer"
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             subCategoryName:
*               type: string
*             categoryId:
*               type: integer
*             createdby:
*               type: integer
*     responses:
*       200:
*         description: Sub-category updated
*       404:
*         description: Sub-category you are updating does not exit
*   delete:
*     tags:
*       - Sub-category
*     summary: To delete Sub-category by Id
*     parameters:
*     - name: "id"
*       in: "path"
*       description: "Id of Sub-category to delete"
*       required: true
*       type: "integer"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Sub-category deleted. 
*       404:    
*         description: The Sub-category does not exit.
*/