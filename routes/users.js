// var express = require('express');
// var router = express.Router();

// /* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

// module.exports = router;

const user = require('../api/controller/users');
const express =require('express');
const checkAuth = require('../api/Middleware/checkAuth')
const errorWrap = require('../utils/errorWrap'); // importing check authentication.

const route = express.Router();

route.post('/',checkAuth,errorWrap.wrapper(user.addUser)); //create user

route.get('/',checkAuth,errorWrap.wrapper(user.readUser)); //api to read user

route.get('/:id',checkAuth,errorWrap.wrapper(user.ReadUserById)); //api to read user by id

route.put('/:id',checkAuth,errorWrap.wrapper(user.UpdateUser)); //api to read user by id

route.delete('/:id',checkAuth,errorWrap.wrapper(user.RemoveUser)); //api to read user by id

module.exports= route;

/**
* @swagger
* /user:
*   get:
*     tags:
*       - user
*     name: Read user
*     summary: To read user with pagination
*     parameters:
*     - name: "search"
*       in: "query"
*       description: "search your keyword"
*       type: "string"
*     - name: "from"
*       in: "query"
*       description: "Pagination starting point"
*       type: "string"
*     - name: "to"
*       in: "query"
*       description: "Pagination ending point"
*       type: "string"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Data found 
*       404:
*         description: Data not found
*   post:
*     tags:
*       - user
*     summary: To add user
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             firstName:
*               type: string
*             middleName:
*               type: string
*             lastName:
*               type: string
*             mobile:
*               type: integer
*             emailId:
*               type: string
*             password:
*               type: integer
*         required:
*           - categoryName
*     responses:
*       201:
*         description: user created
*       422:
*         description: user not created
* /user/{id}:
*   patch:
*     tags:
*       - user
*     summary: To update user
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: "id"
*         in: "path"
*         description: "Id of user to update"
*         required: true
*         type: "integer"
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             firstName:
*               type: string
*             middleName:
*               type: string
*             lastName:
*               type: string
*             mobile:
*               type: integer
*             emailId:
*               type: string
*             password:
*               type: integer
*         required:
*           - categoryName
*     responses:
*       200:
*         description: user updated
*       404:
*         description: user you are updating does not exit
*   delete:
*     tags:
*       - user
*     summary: To delete user by Id
*     parameters:
*     - name: "id"
*       in: "path"
*       description: "Id of user to delete"
*       required: true
*       type: "integer"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: user deleted. 
*       404:    
*         description: The user does not exit.
*/