const imageProduct = require('../api/controller/imageProduct');
const express =require('express');
const checkAuthUser = require('../api/Middleware/checkAuth');
const errorWrap = require('../utils/errorWrap'); // importing check authentication.


const route = express.Router();

route.post('/',checkAuthUser,errorWrap.wrapper(imageProduct.AddBulkImage));//to add notification data

module.exports= route;
