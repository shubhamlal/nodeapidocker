const products = require('../api/controller/products');
const express =require('express');
const errorWrap = require('../utils/errorWrap'); // importing check authentication.

const route = express.Router();

const checkAuth = require('../api/Middleware/checkAuth'); // importing check authentication.

const validationError = require('../api/Middleware/validationError'); // importing validation error middleware.

const  { productValidation, excelValidation }  = require('../api/validation/product'); 

route.post('/',checkAuth,productValidation,validationError,errorWrap.wrapper(products.AddProducts)); // api to create products.

route.get('/',checkAuth,errorWrap.wrapper(products.ReadAllProducts)); // api to read all products.

route.get('/:id',checkAuth,errorWrap.wrapper(products.ReadProductById));//api to read product by id.

route.put('/:id',checkAuth,productValidation,validationError,errorWrap.wrapper(products.UpdateProduct));// Update Product.

route.post('/from-excel',checkAuth,errorWrap.wrapper(products.AddProductThorughExcel));// api to create Product from excel.

route.delete('/:id',checkAuth,errorWrap.wrapper(products.RemoveProduct));//api to remove Update Product.

module.exports = route;


/**
* @swagger
* /products:
*   get:
*     tags:
*       - Product
*     name: Product APIs
*     summary: To read Product with pagination
*     parameters:
*     - name: "search"
*       in: "query"
*       description: "search your keyword"
*       type: "string"
*     - name: "from"
*       in: "query"
*       description: "Pagination starting point"
*       type: "string"
*     - name: "to"
*       in: "query"
*       description: "Pagination ending point"
*       type: "string"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Data found 
*       404:
*         description: Data not found
*   post:
*     tags:
*       - Product
*     summary: To add Product
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             subCategoryId:
*               type: integer
*             sku:
*               type: string
*             productName:
*               type: string
*             height:
*               type: integer
*               format: float
*             price:
*               type: integer
*               format: float
*             purity:
*               type: integer
*             productImage:
*               type: string
*             metaltype:
*               type: string
*         required:
*           - subCategoryId
*           - sku
*           - productName
*           - height
*           - price
*           - purity
*           - productImage
*           - metaltype
*     responses:
*       201:
*         description: Product created
*       422:
*         description: Product not created
* /products/{id}:
*   get:
*     tags:
*       - Product
*     summary: To read Product by Id
*     parameters:
*     - name: "id"
*       in: "path"
*       description: "ID of product to return"
*       required: true
*       type: "integer"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Product found. 
*       404:    
*         description: The product does not exit.
*   put:
*     tags:
*       - Product
*     summary: To update a single Product
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: "id"
*         in: "path"
*         description: "Id of product to update"
*         required: true
*         type: "integer"
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             subCategoryId:
*               type: integer
*             sku:
*               type: string
*             productName:
*               type: string
*             height:
*               type: integer
*               format: float
*             price:
*               type: integer
*               format: float
*             purity:
*               type: integer
*             productImage:
*               type: string
*             metaltype:
*               type: string
*     responses:
*       200:
*         description: Product updated.
*       404:
*         description: The product you are updating does not exit.
*   delete:
*     tags:
*       - Product
*     summary: To delete Product by Id
*     parameters:
*     - name: "id"
*       in: "path"
*       description: "ID of product to delete"
*       required: true
*       type: "integer"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Product deleted. 
*       404:    
*         description: The product does not exit.
*/