const express = require('express');

const router = express.Router();

const checkAuth = require('../api/Middleware/checkAuth');

const users = require('./users');

const category = require("./category");

// const roles = require("./roles");

const signIn = require("./signin");

const subCategory = require("./subCategory");

const products = require('./products');

const wallet = require('./wallet');

const notification = require('./notification');

const paymentType = require('./paymentType');

const bulkUpload = require('./bulkUpload');

const bulkImageUpload = require('./imageProduct');

const productCalculation = require('./productCalculation');

const permissions = require('./permissions')

const roles = require('./roles');

const states = require('./states');

const cities = require('./cities')


router.use('/user',checkAuth,users);

router.use('/category',checkAuth,category);

router.use('/signIn',signIn);

router.use('/sub-category', subCategory);

router.use('/products',products);

router.use('/wallet',wallet);

router.use('/notification',notification);

router.use('/payment-type', paymentType);

router.use('/bulk-upload-file',bulkUpload);

router.use('/bulk-image-upload',bulkImageUpload);

router.use('/products-calculation',productCalculation);

router.use('/roles',roles);

router.use('/permissions',permissions);

router.use('/states',states);

router.use('/cities',cities);

module.exports = router;