var express = require('express');
var router = express.Router();

const { postState, getState } = require('../api/controller/states')
const checkAuth = require('../api/Middleware/checkAuth');
const errorWrap = require('../utils/errorWrap'); // importing check authentication.


const multer = require('multer');
const storage = multer.diskStorage({
    destination: 'public/uploads/csv',
    filename: (req, file, cb) => {
        const extArray = file.originalname.split('.');
        const extension = extArray[extArray.length - 1];
        cb(null, `${Date.now()}.${extension}`)
    }
})
const upload = multer({
    storage: storage
});

router.post('/', checkAuth, upload.single('csv'), errorWrap.wrapper(postState));

router.get('/', checkAuth, errorWrap.wrapper(getState));

module.exports = router;


/**
* @swagger
* /states:
*   get:
*     tags:
*       - States
*     name: States APIs
*     summary: To read States
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Data found 
*       404:
*         description: Data not found
*/