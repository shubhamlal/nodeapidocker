const permissions = require('../api/controller/permissions');
const express =require('express');
const checkAuthUser = require('../api/Middleware/checkAuth');
const  { permissionValidation }  = require('../api/validation/permissions'); 
const validationError = require('../api/Middleware/validationError'); // importing validation error middleware.
const errorWrap = require('../utils/errorWrap'); // importing check authentication.


const route = express.Router();

route.post('/',checkAuthUser,permissionValidation,validationError,errorWrap.wrapper(permissions.CreatePermissions)); //api to get roles

route.get('/',checkAuthUser,errorWrap.wrapper(permissions.GetPermissionData)); //api to get permissions

route.put('/:id',checkAuthUser,permissionValidation,validationError,errorWrap.wrapper(permissions.UpdatePermissions));

route.delete('/:id',checkAuthUser,errorWrap.wrapper(permissions.RemovePermission));

module.exports= route;


/**
* @swagger
* /permissions:
*   get:
*     tags:
*       - permissions
*     name: permissions APIs
*     summary: To read permissions
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Data found 
*       404:
*         description: Data not found
*   post:
*     tags:
*       - permissions
*     summary: To add permissions
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             permissionName:
*               type: string
*             createdby:
*               type: integer
*         required:
*           - permissionName
*     responses:
*       201:
*         description: permissions created
*       422:
*         description: permissions not created
* /permissions/{id}:
*   put:
*     tags:
*       - permissions
*     summary: To update permissions
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: "id"
*         in: "path"
*         description: "Id of permissions to update"
*         required: true
*         type: "integer"
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             permissionName:
*               type: string
*             createdby:
*               type: integer
*     responses:
*       200:
*         description: permissions updated
*       404:
*         description: permissions you are updating does not exit
*   delete:
*     tags:
*       - permissions
*     summary: To delete permissions by Id
*     parameters:
*     - name: "id"
*       in: "path"
*       description: "Id of permissions to delete"
*       required: true
*       type: "integer"
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: permissions deleted. 
*       404:    
*         description: The permissions does not exit.
*/