const productCalculation = require('../api/controller/productCalculation');
const express =require('express');
const errorWrap = require('../utils/errorWrap'); // importing check authentication.

const route = express.Router();

const checkAuth = require('../api/Middleware/checkAuth'); // importing check authentication.

route.get('/',checkAuth,errorWrap.wrapper(productCalculation.GetAllCalculationValues)); //get all Calculation values

route.get('/:id',checkAuth,errorWrap.wrapper(productCalculation.ReadCalculationById));//get all Calculation values bt id

route.put('/:id',checkAuth,errorWrap.wrapper(productCalculation.UpdateCalculationValues));// api to update products calculation values.

route.post('/',checkAuth,errorWrap.wrapper(productCalculation.AddCalcultionValues)); // api to create products calculation values.

module.exports = route;
