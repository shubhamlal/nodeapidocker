const notification = require('../api/controller/notification');

const express =require('express');

const checkAuthUser = require('../api/Middleware/checkAuth')

const errorWrap = require('../utils/errorWrap'); // importing check authentication.


const route = express.Router();

route.post('/',checkAuthUser,errorWrap.wrapper(notification.AddNotification));//to add notification data

route.get('/',checkAuthUser,errorWrap.wrapper(notification.GetNotification)); //to get notification data

module.exports= route;

/**
* @swagger
* /notification:
*   get:
*     tags:
*       - notifications
*     name: notification APIs
*     summary: To read notification type
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Data found 
*       404:
*         description: Data not found
*   post:
*     tags:
*       - notifications
*     summary: To add notifications type
*     security:
*       - bearerAuth: []  
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             notificationType:
*               type: string
*         required:
*           - notificationType
*     responses:
*       201:
*         description: Category created
*       422:
*         description: Category not created
*/