const baseUrl = 'http://127.0.0.1:9000/uploads/bulkUpload/';

module.exports = (sequelize, DataTypes) => {
    const BulkUpload = sequelize.define('bulk_upload', {
        // attributes
        filename: {
            type: DataTypes.TEXT,
            field:'file_name'
        },
        mimetype: {
            type: DataTypes.TEXT,
            field:'mimetype'

        },
        encoding: {
            type: DataTypes.TEXT,
            field:'encoding'

        },
        originalname: {
            type: DataTypes.TEXT,
            field:'original_name'

        },
        url:{
            type: DataTypes.TEXT,
            field:'origiurlnal_name'

        },
        type:{
            type:DataTypes.TEXT,
            field:'type'

        },
        userId:{
            type:DataTypes.INTEGER,
            field:'user_id'
        },
        status: {
            type: DataTypes.STRING,
            defaultValue: "loading"
        }
    }, {
            freezeTableName: true,
            tableName: 'bulk_upload',
        });

        BulkUpload.associate = function (models) {
        BulkUpload.belongsTo(models.users, { foreignKey: 'userId', as: 'user' });
    }

    // This will return required JSON.

    BulkUpload.prototype.toJSON = function () {
        var values = Object.assign({}, this.get());
        values.URL = baseUrl + values.filename;
        values.path = `public/uploads/bulkUpload/${values.filename}`;
        delete values.encoding;
        return values;
    }

    return BulkUpload;
}