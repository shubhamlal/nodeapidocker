
module.exports = (sequelize, DataTypes) => {
    const ProductBulkUpload = sequelize.define('product_bulk_upload', {
        // attributes
        category: {
            type: DataTypes.STRING,
            defaultValue: 0,
            field:'category'
        },
        subCategory: {
            type: DataTypes.STRING,
            defaultValue: 0,
            field:'subcategory'
        },
        sku: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 0,
            field:'sku'
        },
        productName: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 0,
            field:'product_name'
        },
        slug: {
            type: DataTypes.STRING,
            field:'slug'
        },
        status:{
            type:DataTypes.STRING,
            defaultValue: "processing",
        },
        height: {
            type: DataTypes.FLOAT,
            defaultValue: 0,
            field:'height'
        },
        price: {
            type: DataTypes.FLOAT,
            defaultValue: 0,
            field:'price'
        },
        purity: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            field:'purity'
        },
        productImage: {
            type: DataTypes.TEXT,
            defaultValue: 0,
            field:'product_image'
        },
        metaltype: {
            type: DataTypes.TEXT,
            defaultValue: 0,
            field:'metaltype'

        },
        thumbnailImage: {
            type: DataTypes.TEXT,
            defaultValue: 0,
            field:'thumbnail_image'
        },
        createdby:{
            type: DataTypes.TEXT,
            defaultValue: 0,
            field:'createdby'
        },
        fileId:{
            type:DataTypes.INTEGER,
            field:'file_id'
        },
        message:{
            type:DataTypes.TEXT,
            field:'message'
        }
    }, {
        freezeTableName: true,
        tableName: 'product_bulk_upload',
    });

     ProductBulkUpload.associate = function (models) {
    ProductBulkUpload.belongsTo(models.bulk_upload, { foreignKey: 'fileId', as: 'file' });
    }

    
    return ProductBulkUpload;
}
   