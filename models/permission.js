
module.exports = (sequelize, DataTypes) => {
    const permission = sequelize.define('permission', {
        // attributes
        permissionName:{
            type: DataTypes.STRING,
            field:'permission_name'
        },
        slug:{
            type:DataTypes.STRING,
            field:'slug'
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true,
            field:'status'
        },
        createdby:{
            type: DataTypes.INTEGER,
            field:'createdby'
        }
    }, {
            freezeTableName: true,
            allowNull: false,
            tableName: 'permission',
        });


        permission.createPermission = (permissionName,slug,createdby) =>
        permission.create({
            permissionName,slug,createdby
        });
    
        // defining Association
        permission.associate = function(models) {
        permission.belongsToMany(models.roles, {through: models.permission_roles});
        }
    
    


    return permission;
}