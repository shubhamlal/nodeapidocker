
module.exports = (sequelize, DataTypes) => {
    const productSubcategory = sequelize.define('product_subcategory', {
        subCategoryId: {
            type: DataTypes.INTEGER,
            field:'subcategory_id'
        },
        productId: {
            type: DataTypes.INTEGER,
            field:'product_id'  
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true
        }
    }, {
            freezeTableName: true,
            allowNull: false,
            tableName: 'product_subcategory',
        });

        productSubcategory.associate = function (models) {
            productSubcategory.belongsTo(models.subcategories, { foreignKey: 'subCategoryId', as: 'subCategory' });
            productSubcategory.belongsTo(models.products, { foreignKey: 'productId', as: 'products' });
        }

    return productSubcategory;
}

