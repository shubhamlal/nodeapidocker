
module.exports = (sequelize, DataTypes) => {
    const imagesProducts = sequelize.define('images_products', {
        // attributes
       productSkuCode: {
            type: DataTypes.STRING,
            field:'product_sku_code'

        },
        imageName: {
            type: DataTypes.STRING,
            field:'image_name'

        },
        firstImageName:{
            type: DataTypes.STRING,
            field:'first_image_name'
        },
        productId: {
            type:DataTypes.INTEGER,
            field:'product_id'
            },
        
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        },
        createdby:{
            type: DataTypes.TEXT,
            defaultValue: 0
        }
    }, {
            freezeTableName: true,
            allowNull: false,
            tableName: 'images_products',
        });

        imagesProducts.associate = function (models) {
            imagesProducts.belongsTo(models.products, { foreignKey: 'sku', as: 'productSku' });
        }


    return imagesProducts;
}