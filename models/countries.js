const index =require("./index");

module.exports = (sequlize , DataTypes) =>{
    const countries = sequlize.define("countries",
    {
        country_name:{
            type:DataTypes.STRING,
            defaultValue: 0
        },
        slug:{
            type:DataTypes.STRING,
            allowNill:false,
            defaultValue: 0
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true
            },
            
    },
    { 
       freezeTableName: true,
       tableName: "countries"
    });

    countries.associate = function(models) {
        countries.hasOne(models.states);
    }

    return countries;
}