
module.exports = (sequelize, DataTypes) => {
    const tranctions = sequelize.define('tranctions', {
        store_id: {
            type: DataTypes.TEXT,
        },
        user_id:{
            type: DataTypes.INTEGER
        },
        order_status:{
            type:DataTypes.STRING,
        },
        no_of_pending_emi:{
            type:DataTypes.INTEGER,
        },
        emi_start_date: {
            type: DataTypes.DATE,
        },
        emi_end_date: {
            type: DataTypes.DATE,
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        },
    }, {
            freezeTableName: true,
            tableName: 'tranctions',
        }); 
        
        return tranctions;
    }