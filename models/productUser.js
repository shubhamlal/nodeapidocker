
module.exports = (sequelize, DataTypes) => {
    const productUser = sequelize.define('product_user', {

        userId: {
            type: DataTypes.INTEGER,
            field:'user_id'  
        },
        productId: {
            type: DataTypes.INTEGER,
            field:'product_id'
        }
    }, {
            freezeTableName: true,
            allowNull: false,
            tableName: 'product_user',
        });

        productUser.associate = function (models) {
            productUser.belongsTo(models.products, { foreignKey: 'productId', as: 'products' });
            productUser.belongsTo(models.users, { foreignKey: 'userId', as: 'users' });
        }

    return productUser;
}

