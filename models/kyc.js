
module.exports = (sequelize, DataTypes) => {
    const kycDetails = sequelize.define('kyc_details', {
        // attributes
        document_name:{
            type: DataTypes.STRING
        },
        document_image: {
            type: DataTypes.TEXT,
        },
        user_id:{
            type: DataTypes.INTEGER
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        }
    }, {
            freezeTableName: true,
            tableName: 'kyc_details',
        }); 
        
        return kycDetails;
    }