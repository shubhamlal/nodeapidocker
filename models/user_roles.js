
module.exports = (sequelize, DataTypes) => {
    const roleUsers = sequelize.define('user_roles', {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        roleId: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        }
    }, {
            freezeTableName: true,
            roleUsers: 'user_roles',
        }); 
        
        // defining Association
        // roleUsers.associate = function(models) {
        //     roleUsers.belongsTo(models.users);
        //     roleUsers.belongsTo(models.roles);
        //     }
        return roleUsers;
    }