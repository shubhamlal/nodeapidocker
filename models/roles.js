
module.exports = (sequelize, DataTypes) => {
    const Roles = sequelize.define('roles', {
        // attributes
        roleName: {
            type: DataTypes.STRING
        },
        slug:{
            type:DataTypes.STRING,
            allowNill:false,
            defaultValue: 0
        },
        createdBy:{
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        },
    }, {
            freezeTableName: true,
            allowNull: false,
            tableName: 'roles',
        });

        Roles.createrole = (roleName,slug,createdby) =>
        Roles.create({
            roleName,slug,createdby
        });

        // To delete category
        Roles.deleteRole = id =>
        Roles.update({ status: false }, { where: { status: true, id } });

        // defining Association
        Roles.associate = function(models) {
        Roles.belongsToMany(models.users, {through: models.user_roles});
        Roles.belongsToMany(models.permission, { through: models.permission_roles });
        Roles.belongsTo(models.users,{as:'roles',foreignKey:'createdBy'});
        }
    
    


    return Roles;
}