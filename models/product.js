
module.exports = (sequelize, DataTypes) => {
    const product = sequelize.define('products', {
       
        subCategoryId: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            field:'subcategory_Id'
        },
        sku: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 0,
            field:'sku'
        },
        productName: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 0,
            field:'product_name'
        },
        slug: {
            type: DataTypes.STRING,
            field:'slug'

        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true,
        },
        height: {
            type: DataTypes.FLOAT,
            defaultValue: 0,
            field:'height'

        },
        price: {
            type: DataTypes.FLOAT,
            defaultValue: 0,
            field:'price'
        },
        purity: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            field:'purity'

        },
        productImage: {
            type: DataTypes.TEXT,
            defaultValue: 0,
            field:'product_image'
            
        },
        metaltype: {
            type: DataTypes.TEXT,
            defaultValue: 0,
            field:'metaltype'

        },
        // thumbnailImage: {
        //     type: DataTypes.TEXT,
        //     defaultValue: 0,
        //     field:'thumbnail_image'

        // },
        createdby:{
            type: DataTypes.TEXT,
            defaultValue: 0,
            field:'createdby'

        }
       
    }, {
        freezeTableName: true,
        tableName: 'products',
    });
    product.associate = function (models) {
        product.belongsTo(models.subcategories, { foreignKey: 'subCategoryId', as: 'subCategory' });
        // product.belongsTo(models.users, { foreignKey: 'createdby', as: 'users' });
        product.hasMany(models.images_products, { foreignKey: 'productId', as: 'images_products' });
        product.hasMany(models.products_price, {foreignKey:'productId', as:'product_price'});
    }

    // Function to create product
    product.createProduct = ( 
        subCategoryId,
        sku,
        productName,
        height,
        price,
        purity,
        productImage,
        metaltype,
        // thumbnailImage,
        createdby) => product.create({
            subCategoryId,
            sku,
            productName,
            height,
            price,
            purity,
            productImage,
            metaltype,
            // thumbnailImage,
            createdby });

  // Function to update product
  product.updateProduct = (id,
    subCategoryId,
    sku,
    productName,
    height,
    price,
    purity,
    productImage,
    metaltype,
    // thumbnailImage,
    createdby) => product.update({
        subCategoryId,
        sku,
        productName,
        height,
        price,
        purity,
        productImage,
        metaltype,
        // thumbnailImage,
        createdby},{ where: { id: id, status: true } });

        product.removeProduct=(id)=>product.update({status:false},{where:{status:true,id}})

    //Find Product
    product.findUniqueProduct=(sku)=>product.findOne({where:{sku, status:true}});

    return product;
}



// module.exports = (sequelize, DataTypes) => {
//     const Product = sequelize.define('products', {
//         // attributes
//         category_id: {
//             type: DataTypes.INTEGER,
//             defaultValue: 0
//         },
//         sku: {
//             type: DataTypes.STRING,
//             allowNull: false,
//             defaultValue: 0
//         },
//         product_name: {
//             type: DataTypes.STRING,
//             allowNull: false,
//             defaultValue: 0
//         },
//         slug: {
//             type: DataTypes.STRING
//         },
//         status:{
//             type:DataTypes.BOOLEAN,
//             defaultValue: true,
//         },
//         height: {
//             type: DataTypes.FLOAT,
//             defaultValue: 0
//         },
//         price: {
//             type: DataTypes.FLOAT,
//             defaultValue: 0
//         },
//         purity: {
//             type: DataTypes.INTEGER,
//             defaultValue: 0
//         },
//         product_image: {
//             type: DataTypes.TEXT,
//             defaultValue: 0
//         },
//         metatype: {
//             type: DataTypes.TEXT,
//             defaultValue: 0
//         },
//         thumbnail_image: {
//             type: DataTypes.TEXT,
//             defaultValue: 0
//         },
//         createdby:{
//             type: DataTypes.TEXT,
//             defaultValue: 0
//         }
//         // createdAt: {
//         //     type: DataTypes.DATE,
//         //     defaultValue:DataTypes.NOW
//         //     },
//         // updated_at: {
//         //     type: DataTypes.DATE,
//         //     defaultValue:DataTypes.NOW
//         // }
       
//     }, {
//         freezeTableName: true,
//         tableName: 'products',
//     });

  
//     return Product;
// }