
module.exports = (sequelize, DataTypes) => {
    const PaymentType = sequelize.define('payment_types', {
        // attributes
        paymentType: {
            type: DataTypes.STRING,
            field:'payment_type'

        },
        slug:{
            type:DataTypes.STRING,
            defaultValue: 0,
            field:'slug'
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true,
            field:'status'
        }
    }, {
            freezeTableName: true,
            allowNull: false,
            tableName: 'payment_types',
        });
        PaymentType.createPaymentType = (paymentType) =>
        PaymentType.create({paymentType});
        
        PaymentType.readPaymentTypeById = id =>
        PaymentType.findAll({
            where: { id, status: true }
        });

         // To update category
         PaymentType.updatePaymentType = (id, paymentType) =>
         PaymentType.update({ paymentType }, { where: { status: true, id } });

         PaymentType.deletePaymentType = (id) =>
         PaymentType.update({ status: false }, { where: { status: true, id } });
         

    return PaymentType;
}