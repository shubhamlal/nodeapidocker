
module.exports = (sequelize, DataTypes) => {
    const walletprice = sequelize.define('wallet_price', {
        // attributes
        metalType: {
            type: DataTypes.STRING,
            allowNull: false,
            field:'metal_type',
            defaultValue: 0,
            validate: {
                len: {
                    args: [0, 30]
                }
            }
        },
        walletPrice: {
            type: DataTypes.FLOAT,
            field:'wallet_price',
            defaultValue: 0
        },
        forwordCostThreeMonth: {
            type: DataTypes.FLOAT,
            field:'forword_cost_three_month',
            defaultValue: 0
        },
        forwordCostSixMonth: {
            type: DataTypes.FLOAT,
            field:'forword_cost_six_month',
            defaultValue: 0
        },
        forwordCostNineMonth: {
            type: DataTypes.FLOAT,
            field:'forword_cost_nine_month',
            defaultValue: 0

        },
        vat:{
            type: DataTypes.FLOAT,
            field:'vat',
            defaultValue: 0

        },
        excixeDuty:{
            type: DataTypes.FLOAT,
            field:'excixe_duty',
            defaultValue: 0

        },
        cancelValue:{
            type: DataTypes.FLOAT,
            field:'cancel_value',
            defaultValue: 0

        },
         slug:{
            type:DataTypes.STRING,
            allowNill:false,
            defaultValue: 0,
            field:'slug' 
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true
        }
      
    }, {
            freezeTableName: true,
            tableName: 'wallet_price',
        });

        walletprice.createWallet = ( metalType,
            walletPrice,
            forwordCostThreeMonth,
            forwordCostSixMonth,
            forwordCostNineMonth,
            vat,
            excixeDuty,
            cancelValue) =>
            walletprice.create({
                metalType,
                walletPrice,
            forwordCostThreeMonth,
            forwordCostSixMonth,
            forwordCostNineMonth,
            vat,
            excixeDuty,
            cancelValue
        });

           // To update wallet
           walletprice.updateWalletPrice = (
            id,
            walletGoldPrice,
            walletSilverPrice,
            forwordCostThreeMonth,
            forwordCostSixMonth,
            forwordCostNineMonth,
            vat,
            excixeDuty,
            cancelValue) =>
           walletprice.update({  walletGoldPrice,
            walletSilverPrice,
            forwordCostThreeMonth,
            forwordCostSixMonth,
            forwordCostNineMonth,
            vat,
            excixeDuty,
            cancelValue }, { where: { status: true, id } });

    return walletprice;
}