
module.exports = (sequelize, DataTypes) => {
    const categoryUser = sequelize.define('category_user', {
        categoryId: {
            type: DataTypes.INTEGER,
            field:'category_id'
        },
        userId: {
            type: DataTypes.INTEGER,
            field:'user_id'  
        },
        comission: {
            type: DataTypes.FLOAT,
            field:'comission'  
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true
        }
    }, {
            freezeTableName: true,
            allowNull: false,
            tableName: 'category_user',
        });

        categoryUser.associate = function (models) {
            categoryUser.belongsTo(models.categories, { foreignKey: 'categoryId', as: 'category' });
            categoryUser.belongsTo(models.users, { foreignKey: 'userId', as: 'users' });
        }

    return categoryUser;
}

