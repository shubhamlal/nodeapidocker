module.exports = (sequelize, DataTypes) => {
    const permission_roles = sequelize.define('permission_roles', {
        // attributes
        roleId: {
            type: DataTypes.INTEGER,
            field: 'role_id'
        },
        permissionId: {
            type: DataTypes.INTEGER,
            field: 'permission_id'
        },
        isActive: {
            type: DataTypes.BOOLEAN,
            field: 'is_active',
            defaultValue: true,
        }
    }, {
        freezeTableName: true,
        allowNull: false,
        tableName: 'permission_roles'
    });


    


    return permission_roles;
}
