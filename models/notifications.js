
module.exports = (sequelize, DataTypes) => {
    const notifications = sequelize.define('notifications', {
        // attributes
        notificationType:{
            type: DataTypes.STRING,
            field:'notification_type' 
        },
        slug: {
            type: DataTypes.STRING,
            field:'slug' 
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true,
            field:'status' 

        }
    }, {
            freezeTableName: true,
            tableName: 'notifications',
        }); 

        notifications.createNotification = (notificationType) =>
            notifications.create({notificationType});


        return notifications;
    }