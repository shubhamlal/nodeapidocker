const index =require("./index");

module.exports = (sequlize , DataTypes) =>{
    const productPrice = sequlize.define("products_price",
    {
        // productId:{
        //     type:DataTypes.INTEGER,
        //     defaultValue: 0,
        //     field:'product_id',
        // },
        basePrice:{
            type:DataTypes.FLOAT,
            defaultValue: 0,
            field:'base_price',

        },
        productPrice:{
            type:DataTypes.FLOAT,
            defaultValue: 0,
            field:'product_price',
        },
        finalPrice:{
            type:DataTypes.FLOAT,
            defaultValue: 0,
            field:'final_price',
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true
        }
       
    },
    { 
       freezeTableName: true,
       tableName: "product_price"
    });

    return productPrice;
}