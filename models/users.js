module.exports = (sequelize, DataTypes) => {
    const users = sequelize.define('users', {
        // attributes
        
        firstName: {
            type: DataTypes.STRING,
            field:'first_name' 
        },
        lastName: {
            type: DataTypes.STRING,
            field:'last_name'
        },
        middleName: {
            type: DataTypes.STRING, 
            field:'middle_name' 
        },
        mobile: {
            type: DataTypes.BIGINT,
            field:'mobile' 
        },
        emailId: {
            type: DataTypes.STRING,
            field:'email_id' 
            
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true,
            field:'status' 
        },
        password:{
            type:DataTypes.STRING,
            field:'password' 
        }
    }, {
            freezeTableName: true,
            tableName: 'users',
        });

        

        // defining Association
        users.associate = function(models) {
            // associations can be defined here
            users.hasMany(models.address, {
              foreignKey: 'userId',
              as: 'addresses',
              onDelete: 'CASCADE',
            });
        users.belongsToMany(models.roles, {through: models.user_roles });
        }


        users.readUserById=(id)=> users.findOne(
            {
                where:{
                    status:true
                },
                include: [{
                 model: models.address,
                 as: "addresses",
                 where: {
                   status: true
                 },
                 include:[
                 {
                   model:models.countries,
                 },{
                     model:models.states,
                 },{              
                   model:models.cities,
                 }],
             },
             {
             model: models.roles,
             as: "roles",
             where: {
                 status: true
                 },
             include:[{
                 model: models.permission,
                 as: "permissions",
                 where: {
                     status: true
                 },
             }]
             },]
             }
        );   
        
        //Update_User 
        users.updateUser = async (id, firstName,middleName,lastName,mobile,emailId,password) => {
            let userData = await users.findOne({ where: { id: id, status: true } });
            if (userData) {
                let userUpdated = await users.update({ firstName,middleName,lastName,mobile,emailId,password}, { where: { id: id, status: true } });
                return userUpdated;
            }
        }
        //delete user
        users.removeUser = (id) => users.update({ status: false }, { where: { id: id, status: true } });

        return users;
}