
module.exports = (sequelize, DataTypes) => {
    const carts = sequelize.define('carts', {
        // attributes
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        product_id: {
            type: DataTypes.INTEGER
        },
        quantity: {
            type: DataTypes.INTEGER
        },
        cart_total_value: {
            type: DataTypes.FLOAT
        },
        branch_id: {
            type: DataTypes.INTEGER
        },
        partner_id: {
            type: DataTypes.INTEGER
        }
    }, {
            freezeTableName: true,
            tableName: 'carts',
        });

    return carts;
}