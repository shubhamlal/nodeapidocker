const index =require("./index");

module.exports = (sequlize , DataTypes) =>{
    const address = sequlize.define("address",
    {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true
        }
    },
    { 
       freezeTableName: true,
       tableName: "address"
    });

    address.associate = function(models) {
        address.belongsTo(models.countries);
        address.belongsTo(models.states);
        address.belongsTo(models.cities);
    }
    
    

    return address;
}