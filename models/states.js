const index =require("./index");

module.exports = (sequlize , DataTypes) =>{
    const states = sequlize.define("states",
    {
        stateName:{
            type:DataTypes.STRING,
            field:'state_name'
        },
        slug:{
            type:DataTypes.STRING,
            field:'slug'
        },
        status:{ 
            type:DataTypes.BOOLEAN,
            defaultValue: true,
            field:'status'
        },
    },
    { 
       freezeTableName: true,
       tableName: "states"
    });

    states.associate = function(models) {
        states.hasOne(models.cities);
    }
    return states;
}