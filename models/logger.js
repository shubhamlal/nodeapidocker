module.exports = (sequelize, DataTypes) => {
    const Logger = sequelize.define('logger', {
        // attributes
        userId: {
            type: DataTypes.TEXT,
            allowNull: false,
            field:'user_id' 
        },
        token: {
            type: DataTypes.TEXT,
            field:'token' 
        },
        createdDate: {
            type: DataTypes.DATE,
            field:'created_date' 

        },
        expiryDate: {
            type: DataTypes.DATE,
            field:'expiry_date' 
        }
    }, {
        // options
        freezeTableName: true,
        tableName: 'logger',
        timestamps: false
    });

    return Logger;
}