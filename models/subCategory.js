
module.exports = (sequelize, DataTypes) => {
    const subCategory = sequelize.define('subcategories', {
        // attributes
        subCategoryName: {
            type: DataTypes.STRING,
            allowNull: false,
            field:'subcategory_name',
            validate: {
                len: {
                    args: [0, 30]
                }
            }
        },
        categoryId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field:'category_name',
        },
       
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true
        },
        createdby:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'createdby',
        }
    }, {
            freezeTableName: true,
            tableName: 'subcategories',
        });

        subCategory.associate = function (models) {
            subCategory.belongsTo(models.categories, { foreignKey: 'categoryId', as: 'category' });
        }

        //Add_Sub_Category
        subCategory.addSubCategory = (subCategoryName , categoryId , createdby) => subCategory.create({
            subCategoryName , categoryId , createdby
        });


    //Update_Sub_Category
    subCategory.updateSubCategory=(id,subCategoryName , categoryId , createdby)=>subCategory.update({ subCategoryName , categoryId , createdby },{ where: { id: id, status: true } });

        
    //Remove_Sub_Category
    subCategory.removeSubCategory=(id)=>subCategory.update({ status: false }, { where: { id: id, status: true } });

    return subCategory;
}