const index =require("./index");

module.exports = (sequlize , DataTypes) =>{
    const cities = sequlize.define("cities",
    {
        cityName:{
            type:DataTypes.STRING,
            defaultValue: 0,
            field:'city_name'

        },
        // stateId:{
        //     type:DataTypes.STRING,
        //     defaultValue: 0,
        //     field:'state_id'
        // },
        slug:{
            type:DataTypes.STRING,
            field:'slug'
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true
        }
    },
    { 
       freezeTableName: true,
       tableName: "cities"
    }
    );
    return cities;
}