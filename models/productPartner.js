
module.exports = (sequelize, DataTypes) => {
    const productPartner = sequelize.define('product_partner', {
        // attributes
        productId: {
            type: DataTypes.INTEGER,
            field:'prouct_id'  
        },
        partnerId: {
            type: DataTypes.INTEGER,
            field:'partner_id'
        },
        slug:{
            type:DataTypes.STRING,
            allowNill:false,
            defaultValue: 0
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        }
    }, {
            freezeTableName: true,
            allowNull: false,
            tableName: 'product_partner',
        });

        productPartner.associate = function (models) {
            productPartner.belongsTo(models.products, { foreignKey: 'productId', as: 'products' });
            productPartner.belongsTo(models.users, { foreignKey: 'partnerId', as: 'users' });
        }

    return productPartner;
}

