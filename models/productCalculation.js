const index =require("./index");

module.exports = (sequlize , DataTypes) =>{
    const productCalculation = sequlize.define("products_calculation",
    {
        rateOnSystem995:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'rate_on_system_995',
        },
        goldPricePre10Gm:{
            type:DataTypes.FLOAT,
            defaultValue: 0,
            field:'gold_price_pre10gm',

        },
        pureGoldFitness:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'pure_gold_fitness',
        },
        conversionFactor:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'conversion_factor',
        },
        mfgCostPerGm:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'mfg_cost_per_gm',
        },
        hallmarkingPackaging:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'hallmarking_packaging',
        },
        shipping:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'shipping',
        },
         exciseDuty:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'excise_duty',
        },
        gst:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'gst',
        },
        margin:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'margin',
        },
        fwCost3Months:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'fw_cost_3_months',
        },
        fwCost6Months:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'fw_cost_6_months',
        },
        fwCost9Months:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'fw_cost_9_months',
        },
        cancellationPrice:{
            type:DataTypes.INTEGER,
            defaultValue: 0,
            field:'cancellation_price',
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true
        }
    },
    { 
       freezeTableName: true,
       tableName: "product_calculation"
    });

    productCalculation.createProductCalculation = (
        rateOnSystem995,
        goldPricePre10Gm,
        pureGoldFitness,
        conversionFactor,
        mfgCostPerGm,
        hallmarkingPackaging,
        shipping,
        exciseDuty,
        gst,
        margin,
        fwCost3Months,
        fwCost6Months,
        fwCost9Months,
        cancellationPrice
    ) => productCalculation.create({
        rateOnSystem995,
        goldPricePre10Gm,
        pureGoldFitness,
        conversionFactor,
        mfgCostPerGm,
        hallmarkingPackaging,
        shipping,
        exciseDuty,
        gst,
        margin,
        fwCost3Months,
        fwCost6Months,
        fwCost9Months,
        cancellationPrice
    })

    productCalculation.updateWalletPrice = (
        id,
      rateOnSystem995,
      goldPricePre10Gm,
      pureGoldFitness,
      conversionFactor,
      mfgCostPerGm,
      hallmarkingPackaging,
      shipping,
      exciseDuty,
      gst,
      margin,
      fwCost3Months,
      fwCost6Months,
      fwCost9Months,
      cancellationPrice) =>
        productCalculation.update({  rateOnSystem995,
            goldPricePre10Gm,
            pureGoldFitness,
            conversionFactor,
            mfgCostPerGm,
            hallmarkingPackaging,
            shipping,
            exciseDuty,
            gst,
            margin,
            fwCost3Months,
            fwCost6Months,
            fwCost9Months,
            cancellationPrice}, { where: { status: true, id } });

    return productCalculation;
}