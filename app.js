var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require('body-parser')
var cors = require('cors')
const logInfo = require("./api/Middleware/apiLogger"); // api logger middleware.
const swaggerUi = require("swagger-ui-express");
const swagger=require('./swagger'); //importing swagger file


var app = express();


app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swagger.swaggerSpec)); //Swagger _setup


app.use(cors());


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(logInfo); //middleware for apiloggers



var indexRouter = require('./routes/index');

// app.use(morgan('dev'))
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())


app.use((req, res, next)=>{
  // res.header('Access-Control-Allow-Origin','https://wwww.abc.com')
  res.header('Access-Control-Allow-Origin','*');
  res.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization');
  if(req.method == 'OPTIONS'){
      res.header('Access-Control-Allow-Methods','PUT,PATCH,UPDATE,GET,POST,DELETE')
      return res.status(200).json({})
  }
  next();
})

app.use('/api', indexRouter);

// error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });


app.use(function(err, req, res, next) {
  res.status(500).send({ message: "something went wrong" });
});

module.exports = app;
