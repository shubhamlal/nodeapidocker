const { body } = require("express-validator");
const models = require("../../models");
const Sequelize = models.Sequelize;
const Op = Sequelize.Op;

exports.roleValidation = [
  body('roleName')
    .exists().withMessage('Role name is required')
    .custom(async value => {
      return await models.roles.findOne({ where: { 
        roleName: {
          [Op.iLike]: value},
          status: true }}).then(role => {
        if (role) {
          return Promise.reject("role name already exit !");
        }
      })
    })
    
];
