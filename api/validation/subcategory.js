const { body } = require("express-validator");
const models = require("../../models");
const Sequelize = models.Sequelize;
const Op = Sequelize.Op;


exports.subCategoryValidation = [
    body('subCategoryName')
    .exists().withMessage('Sub-category name is required')
    .isLength({ max: 30 })
    .withMessage('Maximum character length is 30')
    .custom(async value => {
      return await models.subcategories.findOne({where:{ subCategoryName: {
        [Op.iLike]: value}, status:true}}).then(subCategory => {
        if (subCategory) {
          return Promise.reject("Sub-Category name already exit !");
        }
      })
    }),
   
    body('categoryId')
    .exists().withMessage('Category Id is required'),
  ];
  
  
//   exports.subCategoryNameValidation = [
//     body('subCategoryName')
//     .isLength({ max: 30 })
//     .withMessage('Maximum character length is 30')
//     .custom(async value => {
//       return await models.subcategories.findOne({where:{ subCategoryName: {
//         [Op.iLike]: value}, status:true}}).then(subCategory => {
//         if (subCategory) {
//           return Promise.reject("Sub-Category name already exhit !");
//         }
//       })
//     }),
//   ];