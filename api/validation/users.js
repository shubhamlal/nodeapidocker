const { body } = require("express-validator");
const models = require("../../models");

exports.userValidation = [

    body('firstName')
    .exists().withMessage('first name is require')
    .custom(async value =>{
        return await models.users.findOne(value).then(user =>{
            if(!/^[a-zA-z]*$/i.test(value)){
                return Promise.reject("Invalid first name")
        }
        })
    }),

    body('lastName')
    .exists().withMessage('last name is require')
    .custom(async value =>{
        return await models.users.findOne(value).then(user =>{
            if(!/^[a-zA-z]*$/i.test(value)){
                return Promise.reject("Invalid first name")
        }
        })
    }),

    body('middleName')
    .exists().withMessage('middle name is require')
    .custom(async value =>{
        return await models.users.findOne(value).then(user =>{
            if(!/^[a-zA-z]*$/i.test(value)){
                return Promise.reject("Invalid first name")
        }
        })
    }),

  body('mobile')
  .exists().withMessage('Mobile is required')
  .custom(async value => {
    return await models.user.findUniqueUser(value).then(user => {
      if (user) {
        return Promise.reject("Mobile number already exit !")
        }else 
        if(!/^[0-9]{10}$/i.test(value)){
          return Promise.reject("Invalid mobile number")
        }
        })
      })
    ]