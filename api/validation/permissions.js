const { body } = require("express-validator");
const models = require("../../models");
const Sequelize = models.Sequelize;
const Op = Sequelize.Op;

exports.permissionValidation = [
  body('permissionName')
    .exists().withMessage('Permission name is required')
    .custom(async value => {
      return await models.permissions.findOne({ where: { 
        permissionName: {
          [Op.iLike]: value},
          status: true }}).then(permission => {
        if (permission) {
          return Promise.reject("Permission name already exit !");
        }
      })
    })
    
];
