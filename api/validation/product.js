const { body } = require("express-validator");
const models = require("../../models");

exports.productValidation = [
  body("sku")
    .exists()
    .withMessage("Product sku code is required")
    .custom(async value => {
      return await models.products.findUniqueProduct(value).then(product => {
        if (product) {
          return Promise.reject("Product's SKU code already exist !");
        }
      });
    }),

  body("subCategoryId")
    .exists()
    .withMessage("Sub-categoryId is required"),

//   body("productName")
//     .exists()
//     .withMessage("Product name is required"),
];

exports.excelValidation=[
  body("fileId")
  .exists()
  .withMessage("FileId is required"),

  body("fileExtension")
  .exists()
  .withMessage("FileExtension is required"),

  body("path")
  .exists()
  .withMessage("Path is required"),
]