// Load required packages

const models = require('../../models');
const redis = require('redis');
const redisConn = require('../../config/redis')
const Sequelize = models.Sequelize;
const Op = Sequelize.Op;

const client = redis.createClient();
// const client = redis.createClient(redisConn.PORT, redisConn.HOST);


// Api Logger function.

module.exports = (req, res, next) => {
    const createdDateTime = new Date();

    let skipUrls = [
        "/api/signIn"
     
    ];
    if (!skipUrls.includes(req._parsedUrl.pathname)) {
        try {
                const checktoken = req.headers.authorization;
           
                const token = req.headers.authorization.split(" ")[1];


            client.get(token, (err, result) => {
                if (err) {
                    res.status(400).json({ message: err })
                } else if (result) {


                    apiLogger(req, token, createdDateTime)
                    next();
                } else {
                    models.logger.findOne({
                        where: {
                            token: token
                        }
                     })
                        .then(loggedInUser => {

                            if (!loggedInUser) {
                                res.status(401).json({ message: "You are not login user" })
                            } else {
                                client.set(token, JSON.stringify(token));
                                const todayEnd = new Date().setHours(23, 59, 59, 999);
                                client.expireat(token, parseInt(todayEnd / 1000));
                                apiLogger(req, token, createdDateTime)
                                next();
                            }
                        }).catch(error => {
                            res.status(400).json({ message: "Wrong Credential" })
                        })
                }
            })
            
        } catch (error) {
            res.status(400).json({ message: "please login first" })
        }
    } else {
        next();
    }
}

let apiLogger = (req, token, createdDateTime) => {
        models.apilogger.create({
            userToken: token,
            url: req.url,
            method: req.method,
            host: req.hostname,
            body: req.body,
            createdAt: createdDateTime
        })
    
}