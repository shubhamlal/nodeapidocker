const jwt = require("jsonwebtoken")

module.exports = (req, res, next)=>{
    try {
        const token = req.headers.authorization.split(" ")[1]
        // console.log(process.env.Jwt_key)
        // const decode = jwt.verify(token,process.env.Jwt_key)
        const decode = jwt.verify(token,"SecretKey5567")
        req.userData = decode
        next();
    }catch(error){
        res.status(401).json({
            message : "Auth Failed"
        })
    }
}