const models = require("../../models");
const sequelize = models.sequelize;
const Sequelize = models.Sequelize;
const Op = Sequelize.Op;

exports.AddCalcultionValues = async(req,res) => {
    const {
        rateOnSystem995,
        goldPricePre10Gm,
        pureGoldFitness,
        conversionFactor,
        mfgCostPerGm,
        hallmarkingPackaging,
        shipping,
        exciseDuty,
        gst,
        margin,
        fwCost3Months,
        fwCost6Months,
        fwCost9Months,
        cancellationPrice
      } = req.body;
    
      let createProductCalculation = await models.products_calculation.createProductCalculation(
        rateOnSystem995,
        goldPricePre10Gm,
        pureGoldFitness,
        conversionFactor,
        mfgCostPerGm,
        hallmarkingPackaging,
        shipping,
        exciseDuty,
        gst,
        margin,
        fwCost3Months,
        fwCost6Months,
        fwCost9Months,
        cancellationPrice
      );
      
      if (!createProductCalculation) {
        res.status(422).json({ message: "Product calculation values are not created" });
      } else {

        let updateData = await models.products_calculation.update(
            { status:false }, {where: {[Op.not]: {id:createProductCalculation.dataValues.id}}});
        res.status(201).json(createProductCalculation);
        console.log(createProductCalculation.dataValues.id);
      }
}

exports.UpdateCalculationValues = async(req,res) =>{
    const id = req.params.id;
    const { rateOnSystem995,
        goldPricePre10Gm,
        pureGoldFitness,
        conversionFactor,
        mfgCostPerGm,
        hallmarkingPackaging,
        shipping,
        exciseDuty,
        gst,
        margin,
        fwCost3Months,
        fwCost6Months,
        fwCost9Months,
        cancellationPrice } = req.body;
    let updateData = await models.products_calculation.update(
     { status:false } , { where: { id } });
        if(updateData){
            let createData = await models.products_calculation.create({
                rateOnSystem995,
        goldPricePre10Gm,
        pureGoldFitness,
        conversionFactor,
        mfgCostPerGm,
        hallmarkingPackaging,
        shipping,
        exciseDuty,
        gst,
        margin,
        fwCost3Months,
        fwCost6Months,
        fwCost9Months,
        cancellationPrice
            })
                if(createData){
                    res.status(200).json(createData);
                }else{
                    res.status(404).json({ message: "Data not found" });
                }

        }else{
            res.status(404).json({ message: "Data not found" }); 
        }
}

exports.GetAllCalculationValues = async(req,res) =>{
    let productCalculationData = await models.products_calculation.findAll({
        where: {
          status: true
        }
      });
      if (productCalculationData) {
        res.status(200).json(productCalculationData);
      } else {
        res.status(404).json({ message: "Data Not found" });
      }
}

exports.ReadCalculationById = async (req, res) => {
    const id = req.params.id;
    let ProductData = await models.products.findOne({
      where: { id, status: true },
      order: [["updatedAt", "DESC"]],
      include: [
        {
          model: models.subcategories,
          as: "subCategory",
          where: {
            status: true
          },
          include:[{
            model:models.categories,
            as: "category"
          }]
        }
      ]
    });
    if (!ProductData) {
      res.status(404).json({ message: "Data not found" });
    } else {
      res.status(200).json(ProductData);
    }
  };

  
  
