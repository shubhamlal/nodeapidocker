const models = require('../../models');
const sequelize = models.sequelize;

const Sequelize = models.Sequelize;
const Op = Sequelize.Op;
const csv = require('csvtojson')


exports.postCity = async(req, res) => {
    const csvFilePath = req.file.path;
    const jsonArray = await csv().fromFile(csvFilePath);

    let checkIfCsvUploaded = await models.cities.findAll();
    if (checkIfCsvUploaded.length > 0) {
        return res.status(404).json({ messgae: "Cities Csv is already Uploaded" })
    }
    await sequelize.transaction(async t => {
        for (var i = 0; i < jsonArray.length; i++) {
            let data = await models.cities.create({ cityName: jsonArray[i].cityName, stateId: jsonArray[i].stateId }, { transaction: t })
        }
    }).then(() => {
        res.status(200).json({ message: "success" })

    }).catch((exception) => {
        return res.status(500).json({
            message: "something went wrong",
            data: exception.message
        });
    })
}

exports.getCity = async(req, res) => {
    const { stateId } = req.params;
    let cities = await models.cities.findAll({
        where: { status: true, stateId: stateId },
        attributes: ['id', 'cityName', 'stateId'],
    });
    res.status(200).json({ message: cities })


}