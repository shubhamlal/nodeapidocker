const models = require('../../models');
const paginationFUNC = require('../../utils/pagination'); // importing pagination function.
const sequelize = models.sequelize;
const Sequelize = models.Sequelize;
const Op = Sequelize.Op;

exports.AddSubCategory = async (req,res) =>{
    const {subCategoryName , categoryId , createdby} = req.body;
    let createdSubCategory = await models.subcategories.addSubCategory(subCategoryName , categoryId , createdby);
    if (!createdSubCategory) {
        res.status(422).json({ message: 'Sub-category not created' });
    } else {
        res.status(201).json(createdSubCategory)
    }  
}

exports.ReadSubCategory = async (req,res) =>{
  
    const { search, offset, pageSize } =
    paginationFUNC.paginationWithFromTo(req.query.search, req.query.from, req.query.to);
const searchQuery={status: true,
    [Op.or]: {
       "$subcategories.subcategory_name$": { [Op.iLike]: search + '%' },
       "$category.category_name$": { [Op.iLike]:  search + '%' }
    },
    }
let SubCategoryData = await models.subcategories.findAll({
    where: searchQuery,
    offset: offset,
    limit: pageSize,
    include: [{
        model: models.categories,
        as: 'category',
        where: { status: true,
            }
    }]
});
let count = await models.subcategories.findAll({
    where: searchQuery,
    include: [{
        model: models.categories,
        as: 'category',
        where:{
            status: true,
        }
    }]
},);
if (SubCategoryData.length === 0) {
    res.status(200).json({
        data: [],
        count: 0
      });
} else {
    res.status(200).json({
        data: SubCategoryData, 
        count: count.length });
}

}

exports.ReadSubCategoryById = async (req, res) => {
   
        const id = req.params.id;
        let SubCategoryData = await models.subcategories.findAll({
            where: { id, status: true },
            order: [["updatedAt", "DESC"]],
            include: [{
                model: models.categories,
                as: 'category',
                where: { status: true,
                     }
            }]
        });
        if (SubCategoryData.length === 0) {
            res.status(404).json({ message: 'Data not found' });
        } else {
            res.status(200).json(SubCategoryData);
        }
};


// Update Sub-Category.

exports.UpdateSubCategory = async (req, res) => {
    const id = req.params.id;
    const { subCategoryName , categoryId , createdby} = req.body;
    let updateData = await models.subcategories.updateSubCategory(id, subCategoryName , categoryId , createdby)
    if (updateData[0] === 0) {
        res.status(404).json({ message: 'Data not found' });
    } else {
        res.status(200).json({ message: 'Success' });
    }
};


exports.RemoveSubCategory = async (req, res) => {
    const id = req.params.id;
    let deleteSubCategory = await models.subcategories.removeSubCategory(id);
    if (!deleteSubCategory[0]) {
        res.status(404).json({ message: 'Data not found' });
    } else {
        // await models.product.update({ isActive: false }, { where: { subCategoryId:id } });
        res.status(200).json({ message: 'Success' });
    }
}