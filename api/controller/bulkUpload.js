const models = require('../../models');
const multer = require("multer");
const paginationFUNC = require('../../utils/pagination'); // importing pagination function.
const Sequelize = models.Sequelize;
const Op = Sequelize.Op;
const xl = require('excel4node'); //for expost excel

const storage = multer.diskStorage({
    filename: (req, file, cb) => {
        const extArray = file.originalname.split(".");
        const extension = extArray[extArray.length - 1];
        cb(null, `${Date.now()}.${extension}`);
    },
    destination: "public/uploads/bulkUpload/"
});

const uploads = multer({
    storage
}).single("avatar");

exports.SaveRequestBulkUploadFiles = async (req, res, next) => {
    try {
        uploads(req, res, async err => {
            if (err) {
                res.status(500);
            }
            req.file.userId = 1;
            req.file.type = "appRequests";
            req.file.url = req.file.path;
            let uploadFile = await models.bulk_upload.create(req.file);
            if (!uploadFile) {
                res.status(400).json({
                    message: "Error while uploading file!"
                });
            } else {
                res.status(200).json({
                    uploadFile
                });
            }
        });
    } catch (error) {
        console.log(error);
    }
};

exports.getBulkUploadFile = async (req, res) =>{
    const { search, offset, pageSize } =
    paginationFUNC.paginationWithFromTo(req.query.search, req.query.from, req.query.to);
    const searchQuery = {
        [Op.or]: {
            "$bulk_upload.original_name$": {
                [Op.iLike]: search + '%'
            },
            "$bulk_upload.type$": {
                [Op.iLike]: search + '%'
            },
            "$bulk_upload.status$": {
                [Op.iLike]: search + '%'
            },
            "$user.first_name$": {
                [Op.iLike]: search + '%'
            }
        }
    }
let FileData = await models.bulk_upload.findAll({
    where:searchQuery,
    order: [
        ['updatedAt', 'DESC']   
    ],
    offset: offset,
    limit: pageSize,
    include: [{
        model: models.users,
        as: 'user',
        attributes:['first_name'],
        where: { status: true}
    }]
});

let count = await models.bulk_upload.findAll({
    where:searchQuery,
    order: [
        ['id', 'ASC']
    ],
    include: [{
        model: models.users,
        as: 'user',
        attributes:['first_name'],
        where: { status: true}
    }]
});
if (!FileData) {
    res.status(404).json({ message: 'Data not found' });
} else {
    res.status(200).json({data:FileData, count:count.length});
}
}

//api to get bulk upload report.
exports.getBulkReport = async(req, res)=>{
const fileId = req.query.fileId;
const fileNum=parseInt(fileId);
let wb = new xl.Workbook(); 
let ws = wb.addWorksheet('report');
const style = wb.createStyle({
font: {
  color: '#000000',
  bold:true
}
});
const error = wb.createStyle({
font: {
  color: '#a71405'
}
});
const success = wb.createStyle({
font: {
  color: '#29591a'
}
});

let  excelFileData=await models.product_bulk_upload.findAll({ where: {fileId: fileNum },
order: [
    ['id', 'ASC']
]
 });
if (!excelFileData) {
    res.status(404).json({ message: 'Data not found' });
} else {
 
    for(let i=0;excelFileData.length>i;i++){

        ws.cell(1,1).string("category").style(style);
        ws.cell(1,2).string("subcategory").style(style);
        ws.cell(1,3).string("sku").style(style);
        ws.cell(1,4).string("product_name").style(style);
        ws.cell(1,5).string("height").style(style);
        ws.cell(1,6).string("price").style(style);
        ws.cell(1,7).string("purity").style(style);
        ws.cell(1,8).string("metaltype").style(style);
        ws.cell(1,9).string("status").style(style);
        ws.cell(1,10).string("message").style(style);
        ws.cell(i+2,1).string(excelFileData[i].category);
        ws.cell(i+2,2).string(excelFileData[i].subCategory);
        ws.cell(i+2,3).string(excelFileData[i].sku);
        ws.cell(i+2,4).string(excelFileData[i].productName);
        ws.cell(i+2,5).number(excelFileData[i].height);
        ws.cell(i+2,6).number(excelFileData[i].price);
        ws.cell(i+2,7).number(excelFileData[i].purity);
        ws.cell(i+2,8).string(excelFileData[i].metaltype);
        if(excelFileData[i].status==="error"){
            ws.cell(i+2,9).string(excelFileData[i].status).style(error);
        } else {
            ws.cell(i+2,9).string(excelFileData[i].status).style(success);
        }
        ws.cell(i+2,10).string(excelFileData[i].message);
    }
    wb.write(`bulkReport${fileId}.xlsx`,res);
}
}
