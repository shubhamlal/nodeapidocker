const models = require("../../models");
const sequelize = models.sequelize;
const Sequelize = models.Sequelize;
const Op = Sequelize.Op;
const paginationFUNC = require("../../utils/pagination"); // importing pagination function.
const csv = require("csvtojson"); // importing csv to json.
const xlsx2json = require("xlsx2json"); // importing xlsx to json.

exports.AddProducts = async (req, res) => {
  const {
   
    subCategoryId,
    sku,
    productName,
    height,
    price,
    purity,
    productImage,
    metaltype,
    // thumbnailImage,
    createdby
  } = req.body;

  let createProduct = await models.products.createProduct(
    
    subCategoryId,
    sku,
    productName,
    height,
    price,
    purity,
    productImage,
    metaltype,
    // thumbnailImage,
    createdby
  );
  if (!createProduct) {
    res.status(422).json({ message: "Product not created" });
  } else {
    res.status(201).json(createProduct);
  }
};

exports.ReadAllProducts = async (req, res) => {
  const { search, offset, pageSize } = paginationFUNC.paginationWithFromTo(
    req.query.search,
    req.query.from,
    req.query.to
  );
  const searchQuery = {
    status: true,
    [Op.or]: {
      "$products.product_name$": {
        [Op.iLike]: search + "%"
      },
      "$products.sku$": {
        [Op.iLike]: search + "%"
      },
     
      "$subCategory.subcategory_name$": {
        [Op.iLike]: search + "%"
      }
    }
  };
  let ProductData = await models.products.findAll({
    where: searchQuery,
    order: [["updatedAt", "DESC"]],
    offset: offset,
    limit: pageSize,
    include: [{
        model: models.subcategories,
        as: "subCategory",
        where: {
          status: true
        },
        include:[{
          model:models.categories,
          as: "category"
        }]
      }
      ,{
        model: models.images_products,
        as: "images_products",
       
      }
    ]
  });
  let count = await models.products.findAll({
    where: { status: true },
    order: [["updatedAt", "DESC"]],
    include: [{
        model: models.subcategories,
        as: "subCategory",
        where: {
          status: true
        },
        include:[{
            model: models.categories,
            as: "category",
            required: false,
            where: {
              status: true
            }
          }],
      }]
  });

  if (!ProductData) {
    res.status(200).json({
      data: [],
      count: 0
    });
  } else {
    res.status(200).json({
      data: ProductData,
      count: count.length
    });
  }
};

// Read Product by product id.

exports.ReadProductById = async (req, res) => {
  const id = req.params.id;
  let ProductData = await models.products.findOne({
    where: { id, status: true },
    order: [["updatedAt", "DESC"]],
    include: [
      {
        model: models.subcategories,
        as: "subCategory",
        where: {
          status: true
        },
        include:[{
          model:models.categories,
          as: "category"
        }]
      }
    ]
  });
  if (!ProductData) {
    res.status(404).json({ message: "Data not found" });
  } else {
    res.status(200).json(ProductData);
  }
};

exports.UpdateProduct = async (req, res) => {
  const id = req.params.id;
  const {
    // categoryId,
    subCategoryId,
    sku,
    productName,
    height,
    price,
    purity,
    productImage,
    metaltype,
    createdby
  } = req.body;

  let updateData = await models.products.updateProduct(
    id,
    // categoryId,
    subCategoryId,
    sku,
    productName,
    height,
    price,
    purity,
    productImage,
    metaltype,
    createdby
  );
  if (updateData[0] === 0) {
    res.status(404).json({ message: "Data not found" });
  } else {
    res.status(200).json({ message: "Success" });
  }
};

exports.RemoveProduct = async (req, res) => {
  const id = req.params.id;
  let deleteProduct = await models.products.removeProduct(id);
  if (!deleteProduct[0]) {
    res.status(404).json({ message: "Data not found" });
  } else {
    res.status(200).json({ message: "Success" });
  }
};

exports.AddProductThorughExcel = async (req, res, next) => {
  const fileId = req.body.fileId;
  try {
    if (req.body.fileExtension === "csv") {
      let dataArray = await csv().fromFile(req.body.path);

      if (dataArray.length != 0) {
        if (dataArray) {
          for (const ele of dataArray) {
            ele.fileId = fileId;
          }

          let products = await models.product_bulk_upload.bulkCreate(
            dataArray,
            {
              returning: true
            }
          );
          if (!products) {
            models.bulk_upload.update(
              { status: "failed" },
              { where: { id: fileId } }
            );
            res.status(422).json({ message: "data not created" });
          } else {
            const categoryData = await models.categories.findAll({
              where: { status: true }
            });
            const subCategoryData = await models.subcategories.findAll({
              where: { status: true },
              include: [
                {
                  model: models.categories,
                  as: "category",
                  where: { status: true }
                }
              ]
            });

            const productData = await models.product_bulk_upload.findAll({
              where: { fileId: fileId }
            });

            const productSkuCode = await models.products.findAll({
              where: { status: true },
              attributes: ["sku"]
            });

            // console.log(productData, categoryData, subCategoryData);

            productData.map(product => {
              let checkProductInExcel = productData.filter(
                data =>
                  data.sku.toLocaleLowerCase() ===
                  product.sku.toLocaleLowerCase()
              );
              if (checkProductInExcel.length <= 1) {
                let checkCategory = categoryData.filter(
                  category =>
                    category.categoryName.toLocaleLowerCase() ===
                    product.category.toLocaleLowerCase()
                );
                if (checkCategory.length != 0) {
                  let checkSubCategory = subCategoryData.filter(
                    subCategory =>
                      subCategory.subCategoryName.toLocaleLowerCase() ===
                      product.subCategory.toLocaleLowerCase()
                  );

                  if (checkSubCategory.length != 0) {
                    let checkSubCategoryCategory = subCategoryData.filter(
                      subCategory => {
                        if (
                          subCategory.category.categoryName.toLocaleLowerCase() ===
                            product.category.toLocaleLowerCase() &&
                          subCategory.subCategoryName.toLocaleLowerCase() ===
                            product.subCategory.toLocaleLowerCase()
                        ) {
                          return product;
                        }
                      }
                    );

                    if (checkSubCategoryCategory.length != 0) {
                      let checkProductCode = productSkuCode.filter(
                        productCode =>
                          productCode.sku.toLocaleLowerCase() ===
                          product.sku.toLocaleLowerCase()
                      );
                      if (checkProductCode.length == 0) {
                        models.product_bulk_upload.update(
                          { status: "success" },
                          { where: { id: product.id } }
                        );
                        models.products.create({
                          categoryId: checkCategory[0].id,
                          subCategoryId: checkSubCategory[0].id,
                          productName: product.productName,
                          sku: product.sku,
                          height: product.height,
                          price: product.price,
                          purity: product.purity,
                          productImage: product.productImage,
                          metaltype: product.metaltype,
                          thumbnailImage: product.thumbnailImage,
                          createdby: product.createdby
                        });

                        // res.status(200).json({ message: "File uploaded" });
                      } else {
                        models.product_bulk_upload.update(
                          {
                            status: "error",
                            message: "product code already exists"
                          },
                          { where: { id: product.id } }
                        );
                      }
                    } else {
                      models.product_bulk_upload.update(
                        {
                          status: "error",
                          message:
                            "Subcategory does not belong to given category"
                        },
                        { where: { id: product.id } }
                      );
                    }
                  } else {
                    models.product_bulk_upload.update(
                      {
                        status: "error",
                        message: "incorrect subCategory name"
                      },
                      { where: { id: product.id } }
                    );
                  }
                } else {
                  models.product_bulk_upload.update(
                    { status: "error", message: "incorrect category name" },
                    { where: { id: product.id } }
                  );
                }
              } else {
                models.product_bulk_upload.update(
                  {
                    status: "error",
                    message: "duplicate product code entry in document"
                  },
                  { where: { id: product.id } }
                );
              }
            });

            await models.bulk_upload.update(
              { status: "completed" },
              { where: { id: fileId } }
            );
            res.status(200).json({ message: "File uploaded" });
          }
        }
      } else {
        await models.bulk_upload.update(
          { status: "error" },
          { where: { id: fileId } }
        );
        res.status(422).json({ message: "Incorrect excel file" });
      }
    } else if (req.body.fileExtension === "xlsx") {
      let data = await xlsx2json(req.body.path);
      const dataArray = data[0];
      if (dataArray.length != 0) {
        const finalArray = await dataArray.reduce(
          (object, item, index) => {
            if (index === 0) {
              object.mapper = item;
              return object;
            }
            const data = {};
            Object.keys(item).forEach(key => {
              data[object.mapper[key].replace(/\s/g, "")] = item[key];
            });
            object.data.push(data);
            return object;
          },
          { mapper: {}, data: [] }
        );

        if (finalArray.data) {
          for (const ele of finalArray.data) {
            ele.fileId = fileId;
          }
          let products = await models.product_bulk_upload.bulkCreate(
            finalArray.data,
            { returning: true }
          );
          if (!products) {
            models.bulk_upload.update(
              { status: "failed" },
              { where: { id: fileId } }
            );
            res.status(422).json({ message: "data not created" });
          } else {
            const categoryData = await models.categories.findAll({
              where: { status: true }
            });
            const subCategoryData = await models.subcategories.findAll({
              where: { status: true },
              include: [
                {
                  model: models.categories,
                  as: "category",
                  where: { status: true }
                }
              ]
            });
            const productData = await models.product_bulk_upload.findAll({
              where: { fileId: fileId }
            });
            const productSkuCode = await models.products.findAll({
              where: { status: true },
              attributes: ["sku"]
            });

            productData.map(product => {
              let checkProductInExcel = productData.filter(
                data =>
                  data.sku.toLocaleLowerCase() ===
                  product.sku.toLocaleLowerCase()
              );
              if (checkProductInExcel.length <= 1) {
                let checkCategory = categoryData.filter(
                  category =>
                    category.categoryName.toLocaleLowerCase() ===
                    product.category.toLocaleLowerCase()
                );
                if (checkCategory.length != 0) {
                  let checkSubCategory = subCategoryData.filter(
                    subCategory =>
                      subCategory.subCategoryName.toLocaleLowerCase() ===
                      product.subCategory.toLocaleLowerCase()
                  );
                  if (checkSubCategory.length != 0) {
                    let checkSubCategoryCategory = subCategoryData.filter(
                      subCategory => {
                        if (
                          subCategory.category.categoryName.toLocaleLowerCase() ===
                            product.category.toLocaleLowerCase() &&
                          subCategory.subCategoryName.toLocaleLowerCase() ===
                            product.subCategory.toLocaleLowerCase()
                        ) {
                          return product;
                        }
                      }
                    );
                    if (checkSubCategoryCategory.length != 0) {
                      let checkProductCode = productSkuCode.filter(
                        productCode =>
                          productCode.sku.toLocaleLowerCase() ===
                          product.sku.toLocaleLowerCase()
                      );
                      if (checkProductCode.length == 0) {
                        models.product_bulk_upload.update(
                          { status: "success" },
                          { where: { id: product.id } }
                        );
                        models.products.create({
                          categoryId: checkCategory[0].id,
                          subCategoryId: checkSubCategory[0].id,
                          productName: product.productName,
                          sku: product.sku,
                          height: product.height,
                          price: product.price,
                          purity: product.purity,
                          productImage: product.productImage,
                          metaltype: product.metaltype,
                          thumbnailImage: product.thumbnailImage,
                          createdby: product.createdby
                        });
                      } else {
                        models.product_bulk_upload.update(
                          {
                            status: "error",
                            message: "product code already exists"
                          },
                          { where: { id: product.id } }
                        );
                      }
                    } else {
                      models.product_bulk_upload.update(
                        {
                          status: "error",
                          message:
                            "Subcategory does not belong to given category"
                        },
                        { where: { id: product.id } }
                      );
                    }
                  } else {
                    models.product_bulk_upload.update(
                      {
                        status: "error",
                        message: "incorrect subCategory name"
                      },
                      { where: { id: product.id } }
                    );
                  }
                } else {
                  models.product_bulk_upload.update(
                    { status: "error", message: "incorrect category name" },
                    { where: { id: product.id } }
                  );
                }
              } else {
                models.product_bulk_upload.update(
                  {
                    status: "error",
                    message: "duplicate product code entry in document"
                  },
                  { where: { id: product.id } }
                );
              }
            });
            await models.bulk_upload.update(
              { status: "completed" },
              { where: { id: fileId } }
            );
            res.status(200).json({ message: "File uploaded" });
          }
        }
      } else {
        await models.bulk_upload.update(
          { status: "error" },
          { where: { id: fileId } }
        );
        res.status(422).json({ message: "Incorrect excel file" });
      }
    }
  } catch (err) {
    console.log(err);
    await models.bulk_upload.update(
      { status: "error" },
      { where: { id: fileId } }
    );
    res.status(422).json({ message: "Failed to upload products" });
  }
};
