
const models = require('../../models');
const paginationFUNC = require('../../utils/pagination'); // importing pagination function.
const sequelize = models.sequelize;
const Sequelize = models.Sequelize;
const Op = Sequelize.Op;


exports.AddCategory = async (req, res) => {
    const { categoryName, createdby } = req.body;
    const slug = categoryName.toLowerCase().split(" ").join("-");
   
    let createdCategory = await models.categories.createCategory(categoryName,slug,createdby);
    if (!createdCategory) {
      res.status(422).json({ message: "Category not created" });
    } else {
      res.status(201).json(createdCategory);
    }
  };

  exports.getAllCategory = async(req,res) =>{

   
        const { search, offset, pageSize } = paginationFUNC.paginationWithFromTo(
          req.query.search,
          req.query.from,
          req.query.to
        );
        let CategoryData = await models.categories.findAll({
          where: {
            [Sequelize.Op.or]: {
                categoryName: { [Sequelize.Op.iLike]: search + "%" }
            },
            status: true,

          },
          
          offset: offset,
          limit: pageSize
        });
        let count = await models.categories.findAll({
          where: {
            status: true
          }
        });
          res.status(200).json({
            data: CategoryData,
            count: count.length
          });
  }


  // Remove Category.
exports.deleteAnyOneCategory = async (req, res) => {

  const id = req.params.id;
  console.log(id);
  let deleteCategory = await models.categories.update(
    { status: false },{ where: { status: true, id }});
  await models.subcategories.update({ status: false }, { where: { categoryId:id } });
 
  if (!deleteCategory == true) {
      res.status(404).json({ message: 'Data not found' });
  } else {
      res.status(200).json({ message: 'Success' });
  }
}

// Update Category.
exports.UpdateCategory = async (req, res) => {
  const id = req.params.id;
  const { categoryName,createdby } = req.body;
  const slug = categoryName.toLowerCase().split(" ").join("-");
  console.log(slug);
  let updateData = await models.categories.update({ categoryName, createdby, slug }, {
    where: {
      id : `${id}`
    }
  });
  if (updateData === 0) {
      res.status(404).json({ message: 'Data not found' });
  } else {
      // cache(id);
      res.status(200).json({ message: 'Success' });
  }
}



