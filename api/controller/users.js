
const models = require('../../models');
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const saltRounds = 10;
const paginationFUNC = require('../../utils/pagination'); // importing pagination function.
const sequelize = models.sequelize;
const Sequelize = models.Sequelize;
const Op = Sequelize.Op;


exports.addUser = async(req,res)=>{
    const {firstName,middleName,lastName,mobile,emailId,password, countryId, stateId, cityId, roleId,permissionId} = req.body;
    // const {country_id, state_id, city_id} = req.body;
    // const { roleId } = req.body.roleId;
    // console.log({firstName,middleName,lastName,mobile,emailId,password, country_id, state_id, city_id, roleId});

    const emailExist = models.users.findAll({
        where:{
            email_id : emailId,
        }
    }).then( emailResult => {
        if(emailResult.length > 0){
            return res.status(409).json({
                message: "User Exist"
            })
        }else{
            const planTextPassword = password;
            var hashPassword = bcrypt.hash(planTextPassword, saltRounds).then(result => {
                const password = result
                let createdUser = models.users.
                create({firstName,middleName,lastName,mobile,emailId, status:true,password })
                .then( result => {
                    const userId = result.id;
                    // console.log({countryId, stateId, cityId, userId });
                    let createdAddress = models.address.
                create({countryId, stateId, cityId, userId })
                .then( addressResult => {
                    const userId = result.id;
                    const roleId = req.body.roleId;
                    console.log(roleId);
                    let user_role = models.user_roles.
                    create({status:true,userId,roleId})
                    .then( result => {
                        let permission_roles = models.permission_roles.
                        create({status:true,roleId,permissionId})
                        .then( rolePermission => {
                            console.log(rolePermission);
                            console.log(rolePermission);
                            res.status(200).json({ message: 'User Created' });
                        })
                        
                    });
                    // console.log({firstName,middleName,lastName,mobile,emailId,password, country_id, state_id, city_id, roleId});
                    
                })
                }).catch( error => {
                    res.status(422).json({ message: 'User Not Created' });
                });

            }).catch( error => {
                res.status(422).json({ message: 'User not created' });
            });
        }
    })
}


// exports.readUser = async(req,res) =>{
//         users.findAll({ include: [ models.roles ] }).then(users => {
//         console.log(JSON.stringify(users));
//         console.log("Hello");
    
//         /*
//         [{
//             "name": "John Doe",
//             "id": 1,
//             "createdAt": "2013-03-20T20:31:45.000Z",
//             "updatedAt": "2013-03-20T20:31:45.000Z",
//             "tasks": [{
//             "name": "A Task",
//             "id": 1,
//             "createdAt": "2013-03-20T20:31:40.000Z",
//             "updatedAt": "2013-03-20T20:31:40.000Z",
//             "userId": 1
//             }]
//         }]
//         */
//     });
// }

  



// Read User.
exports.readUser = async(req,res) =>{
    const{search,offset , pageSize } = paginationFUNC.paginationWithFromTo(req.query.search, req.query.from, req.query.to);
    const searchQuery = {
        [Op.or]:{
            firstName:{[Op.iLike]:search + '%'},
            middleName: { [Op.iLike]: search + '%' },
            lastName: { [Op.iLike]: search + '%' },
            emailId: { [Op.iLike]: search + '%' },
            mobile: sequelize.where(
                sequelize.cast(sequelize.col('users.mobile'), 'varchar'),
                { [Op.iLike]: search + '%' }
            )
        }
    }
    let userData = await models.users.findAll(
        {
           where:searchQuery,
           offset:offset,
           limit: pageSize,
           include: [{
            model: models.address,
            as: "addresses",
            where: {
              status: true
            },
            include:[
            {
              model:models.countries,
            },{
                model:models.states,
            },{              
              model:models.cities,
            }],
        },
        {
        model: models.roles,
        as: "roles",
        where: {
            status: true
            },
        include:[{
            model: models.permission,
            as: "permissions",
            where: {
                status: true
            },
        }]
        },]
        });
    let count = await models.users.findAll({
        where: searchQuery,
    });
    if (!userData) {
        res.status(404).json({ message: 'Data not found' });
    } else {
        var i = 0;
        for(i; i < userData.length; i++){
            userData[i].password = null;
        }
        userData[0].password = null;
        res.status(200).json({
            data: userData,
            count: count.length
        });
    }
}

//Read User By ID
exports.ReadUserById = async(req , res)=>{
    const id = req.params.id;
    // let userData = await models.users.readUserById(id);
    let userData = await models.users.findOne(
        {
            where:{
                id:id,
                status:true
            },
            include: [{
             model: models.address,
             as: "addresses",
             where: {
               status: true
             },
             include:[
             {
               model:models.countries,
             },{
                 model:models.states,
             },{              
               model:models.cities,
             }],
         },
         {
         model: models.roles,
         as: "roles",
         where: {
             status: true
             },
         include:[{
             model: models.permission,
             as: "permissions",
             where: {
                 status: true
             },
         }]
         },]
         }
    ); 
    console.log(userData);
    if(userData === null){
        return res.status(404).json({ message: 'Data not found' });
    }else{
        return res.status(200).json({userData:userData});
    }
};

// Update User.
exports.UpdateUser = async (req, res) => {
    const id = req.params.id;
    const { firstName,middleName,lastName,mobile,emailId,customerId,password } = req.body;
    let updateData = await models.users.updateUser(id,firstName,middleName,lastName,mobile,emailId,customerId,password);
    if (updateData[0] === 0) {
        res.status(404).json({ message: 'Data not found' });
    } else {
        // cache(id);
        res.status(200).json({ message: 'Success' });
    }
}

// Remove User.
exports.RemoveUser = async (req, res) => {

    const id = req.params.id;
    let deleteUser = await models.users.removeUser(id);
    if (!deleteUser[0]) {
        res.status(404).json({ message: 'Data not found' });
    } else {
        res.status(200).json({ message: 'Success' });
    }
}

// Sigin User.
// exports.signInAdmin = async (req, res) => {

//     const email = "admin@augmount.com";
//     const password = "admin@123";
//     // let deleteUser = await models.users.removeUser(id);
//     if(req.body.email == email && req.body.password == password){
//             const token = jwt.sign({
//                 email : "admin@augmount.com",
//                 _id : 'abcd1234'
//             },
//             "SecretKey5567",
//             {
//                 expiresIn : '1h'
//             });
//         res.status(200).json({ message: 'Success', token: token });
//     }else{
//         models.users.findOne({
//             where:{
//                 email_id: req.body.email
//             }
//         }).then(result => {
//             // console.log(result.password)
//             const password = req.body.password;
//             const getHashPassword = result.password;
//             const email = req.body.email;
//             bcrypt.compare(password, getHashPassword).then( passwordMatch => {
//                 // console.log(passwordMatch);
//                 if(passwordMatch === true){
//                         const token = jwt.sign({
//                             email : req.body.email,
//                             _id : 'abcd1234'
//                             },
//                             "SecretKey5567",
//                             {
//                                 expiresIn : '1h'
//                             });
//                     return res.status(200).json({message: "Authenticate Successfully", token: token})
//                 }else {
//                     return res.status(404).json({message: 'Wrong UserId or Password'});
//                 }
//             }).catch( error => {
//                 return res.status(404).json({message: 'Wrong UserId or Password'});
//             })
//         })
//         .catch( error => {
//             return res.status(404).json({message: 'Wrong UserId or Password'});
//         })
//     } 
// }   

exports.signInAdmin = async (req, res) => {

    const email = "admin@augmount.com";
    const password = "admin@123";
    // let deleteUser = await models.users.removeUser(id);
        if(req.body.email == email && req.body.password == password){
        const token = jwt.sign({
        email : "admin@augmount.com",
        _id : 'abcd1234'
        },
        "SecretKey5567",
        {
        expiresIn : '5h'
        });
    const decoded = jwt.verify(token, "SecretKey5567");
    const createdTime = new Date(decoded.iat * 1000).toGMTString();
    const expiryTime = new Date(decoded.exp * 1000).toGMTString();
    
    await models.logger.create({
    userId: decoded._id,
    token: token,
    expiryDate: expiryTime,
    createdDate: createdTime
    });
    
    
    res.status(200).json({ message: 'Success', token: token });
    }else{
    models.users.findOne({
    where:{
    email_id: req.body.email
    }
    }).then(result => {
    // console.log(result.password)
    const password = req.body.password;
    const getHashPassword = result.password;
    const email = req.body.email;
    bcrypt.compare(password, getHashPassword).then( passwordMatch => {
    // console.log(passwordMatch);
    if(passwordMatch === true){
    const token = jwt.sign({
    email : req.body.email,
    _id : 'abcd1234'
    },
    "SecretKey5567",
    {
    expiresIn : '1h'
    });
    return res.status(200).json({message: "Authenticate Successfully", token: token})
    }else {
    return res.status(404).json({message: 'Wrong UserId or Password'});
    }
    }).catch( error => {
    return res.status(404).json({message: 'Wrong UserId or Password'});
    })
    })
    .catch( error => {
    return res.status(404).json({message: 'Wrong UserId or Password'});
    })
    }
    } 
