const models = require('../../models');
const multer = require('multer');

const storage = multer.diskStorage({
    filename: (req, file, cb) => {
        const extArray = file.originalname.split('.');
        const extension = extArray[extArray.length - 1];
        cb(null, `${Date.now()}.${extension}`);
    },
    destination: 'public/uploads/images/',
});

const uploads = multer({ storage }).single('avatar');

const multipleUploads = multer({ storage }).array('avatar');


exports.AddBulkImage = async(req,res) =>{
   
    multipleUploads(req, res, async (err) => {
        var report={
            error:[],
            success:[]
        }
            if (err) {
                res.status(500);
            }
            const baseUrl = 'http://127.0.0.1:9000/uploads/images/';
            const productData = await models.products.findAll({ where: { status: true }, attributes: ['sku'] });
            
            const uploadedImages=req.files;
           await uploadedImages.map(async(images)=>{
              
                imageName=images.originalname.split('.');
                
                let firstImageName=imageName[0];
                firstImageNameSplit=firstImageName.split('_');
                skuCode=firstImageNameSplit[0];
                imageNumber=firstImageNameSplit[1];
                let checkProductCode=productData.filter(products => products.sku.toLocaleLowerCase()===skuCode.toLocaleLowerCase());
                if(checkProductCode.length==0){
                    report.error.push(images.originalname);
                    return  res.status(500).json({report});
                }else{
                    if(imageNumber=="1"){
                        models.products.update({ productImage:baseUrl+images.filename }, { where: { sku: skuCode } });
                        report.success.push(images.originalname);
                        return  res.status(200).json({report});
                    } else if(imageNumber !== "1"){
                       
                        const getProductImageData = await  models.images_products.findOne({ where: { firstImageName: firstImageName } });
                        
                        if(getProductImageData){

                            let data = await models.images_products.update({ imageName:baseUrl+images.filename }, { where: { firstImageName: firstImageName } });
                            // console.log(data)
                            report.success.push(images.originalname);
                            return  res.status(200).json({report});
                        
                        }else{
                            const productId = await models.products.findOne({where: { sku: skuCode }});
                            console.log(productId.dataValues.id);

                                models.images_products.create({ 
                                imageName:baseUrl+images.filename,
                                productSkuCode: skuCode,
                                firstImageName: firstImageName,
                                productId:productId.dataValues.id
                            });
                                
                                report.success.push(images.originalname);
                                return  res.status(200).json({report});
                            }
                      
                    } else {
                        report.error.push(images.originalname);
                        return  res.status(500).json({report});
                    }
                }
                
            });
            res.status(200).json({report});        
        });
}