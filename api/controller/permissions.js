const models = require('../../models');


exports.CreatePermissions = async (req, res) => {
    const { permissionName, createdby } = req.body;
    const slug = permissionName.toLowerCase().split(" ").join("-");
   
    let createdPermission = await models.permissions.createPermission(permissionName,slug,createdby);
    if (!createdPermission) {
      res.status(422).json({ message: "Category not created" });
    } else {
      res.status(201).json(createdPermission);
    }
  };

  exports.GetPermissionData = async (req, res) =>{
    let permissionData = await models.permissions.findAll({
      where: {
        status: true
      }
    });
    if (permissionData) {
      res.status(200).json(permissionData);
    } else {
      res.status(404).json({ message: "Data Not found" });
    }
  }

  exports.UpdatePermissions = async(req,res) =>{
    const id = req.params.id;
    const { permissionName, createdby } = req.body;
    const slug = permissionName.toLowerCase().split(" ").join("-");
    let updateData = await models.permissions.update({ permissionName, createdby, slug }, {
      where: {
        id : `${id}`
      }
    });
    if (updateData === 0) {
        res.status(404).json({ message: 'Data not found' });
    } else {
        // cache(id);
        res.status(200).json({ message: 'Success' });
    }
  }

  exports.RemovePermission = async(req,res) =>{
    const id = req.params.id;
    let deletePermission =  await models.permissions.update({ status: false }, { where: { id } });
    if (!deletePermission[0]) {
      res.status(404).json({ message: "Data not found" });
    } else {
      res.status(200).json({ message: "Success" });
    }
  }