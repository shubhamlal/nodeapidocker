const models = require('../../models');


exports.AddWalletPrice = async (req, res) => {
    const { metalType,
      walletPrice,
         forwordCostThreeMonth,
         forwordCostSixMonth,
         forwordCostNineMonth,
         vat,
         excixeDuty,
         cancelValue } = req.body;
    let createWallet = await models.wallet_price.createWallet(
      metalType,
      walletPrice,
        forwordCostThreeMonth,
        forwordCostSixMonth,
        forwordCostNineMonth,
        vat,
        excixeDuty,
        cancelValue);
    if (!createWallet) {
      res.status(422).json({ message: "wallet not created" });
    } else {
      res.status(201).json(createWallet);
    }
  };

  exports.GetWalletData = async (req, res) => {
    let walletData = await models.wallet_price.findAll({
      where: {
        status: true
      }
    });
    if (walletData) {
      res.status(200).json(walletData);
    } else {
      res.status(404).json({ message: "Data Not found" });
    }
  };

  exports.UpdateWallet = async (req, res) => {
    const id = req.params.id;
    const { metalType,
      walletPrice,
      forwordCostThreeMonth,
      forwordCostSixMonth,
      forwordCostNineMonth,
      vat,
      excixeDuty,
      cancelValue } = req.body;
    let updateData = await models.wallet_price.updateWalletPrice(
      id,
      metalType,
      walletPrice,
      forwordCostThreeMonth,
      forwordCostSixMonth,
      forwordCostNineMonth,
      vat,
      excixeDuty,
      cancelValue);
    if (updateData[0] === 0) {
      res.status(404).json({ message: "Data not found" });
    } else {
      res.status(200).json({ message: "Success" });
    }
  };