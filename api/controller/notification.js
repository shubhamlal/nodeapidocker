const models = require('../../models');

exports.AddNotification = async (req, res) => {
    const {notificationType} = req.body;
    let notificationData = await models.notifications.createNotification(
        notificationType);
    if (!notificationData) {
      res.status(422).json({ message: "data created" });
    } else {
      res.status(201).json(notificationData);
    }
  };

  exports.GetNotification = async (req,res) =>{
    let notificationData = await models.notifications.findAll({
        where: {
          status: true
        }
      });
      if (notificationData) {
        res.status(200).json(notificationData);
      } else {
        res.status(404).json({ message: "Data Not found" });
      }
  }