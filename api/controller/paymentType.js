const models = require('../../models');

exports.addPaymentType = async (req, res) => {
    const {paymentType} = req.body;
    let paymentData = await models.payment_types.createPaymentType(
      paymentType);
    if (!paymentData) {
      res.status(422).json({ message: "data created" });
    } else {
      res.status(201).json(paymentData);
    }
  };

  exports.GetPaymentType = async (req,res) =>{
    let paymentData = await models.payment_types.findAll({
        where: {
          status: true
        }
      });
      if (paymentData) {
        res.status(200).json(paymentData);
      } else {
        res.status(404).json({ message: "Data Not found" });
      }
  }
  
  exports.GetPaymentTypeById = async(req,res) => {
    const id = req.params.id;
  let paymentData = await models.payment_types.readPaymentTypeById(id);
  if (paymentData.length === 0) {
    res.status(404).json({ message: "Data not found" });
  } else {
    res.status(200).json(paymentData);
  }
  }

  exports.UpdatePaymentType = async (req, res) => {
    const id = req.params.id;
    const { paymentType } = req.body;
    let updateData = await models.payment_types.updatePaymentType(id, paymentType);
    if (updateData[0] === 0) {
      res.status(404).json({ message: "Data not found" });
    } else {
      res.status(200).json({ message: "Success" });
    }
  };

  exports.RemovePaymentType = async(req,res) =>{
    const id = req.params.id;
    let deletepaymentData = await models.payment_types.deletePaymentType(id);
    if (!deletepaymentData[0]) {
      res.status(404).json({ message: "Data not found" });
    } else {
      res.status(200).json({ message: "Success" });
    }
  }