const models = require('../../models');


exports.CreateRole = async (req, res) => {
    const { roleName, createdby } = req.body;
    const slug = roleName.toLowerCase().split(" ").join("-");
   
    let createdRole = await models.roles.createrole(roleName,slug,createdby);
    if (!createdRole) {
      res.status(422).json({ message: "Category not created" });
    } else {
      res.status(201).json(createdRole);
    }
  };

  exports.GetRolesData = async (req, res) => {
    let roleData = await models.roles.findAll({
      where: {
        status: true
      }
    });
    if (roleData) {
      res.status(200).json(roleData);
    } else {
      res.status(404).json({ message: "Data Not found" });
    }
  };


exports.GetRoleById = async (req, res) => {
    const id = req.params.id;
    let roleData = await models.roles.findAll({
        where: { id, status: true },
    });
    if (roleData.length === 0) {
        res.status(404).json({ message: 'Data not found' });
    } else {
        res.status(200).json(roleData);
    }
};

exports.UpdateRole = async (req, res) => {
    const id = req.params.id;
    const { roleName, createdby } = req.body;
    const slug = roleName.toLowerCase().split(" ").join("-");
    let updateData = await models.roles.update({ roleName, createdby, slug }, {
      where: {
        id : `${id}`
      }
    });
    if (updateData === 0) {
        res.status(404).json({ message: 'Data not found' });
    } else {
        // cache(id);
        res.status(200).json({ message: 'Success' });
    }
  }

  
exports.RemoveRole = async (req, res) => {
    const id = req.params.id;
    let deleteRole = await models.roles.update({ status: false }, { where: { id } });
    if (!deleteRole[0]) {
      res.status(404).json({ message: "Data not found" });
    } else {
      res.status(200).json({ message: "Success" });
    }
  };
  
